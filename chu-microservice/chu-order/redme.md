1. 订单表设计
>>>>>-----OrderMaster > +---- 订单主表,对应用户的一笔订单操作--->>>>||
>++>>-----MerchantOrder > +--- 商户订单表(以shopId进行拆分),用户一笔订单中可能存在多店铺订单,商家优惠快照>>+--->>||
>++>>-----OrderDetail > +--- 订单商品详情表(以goodsId进行拆分),商户订单中的商品价格等明细快照>>>end

1、在秒杀的情况下，肯定不能如此高频率的去读写数据库，会严重造成性能问题的
必须使用缓存，将需要秒杀的商品放入缓存中，并使用锁来处理其并发情况。
当接到用户秒杀提交订单的情况下，先将商品数量递减（加锁/解锁）后再进行其他方面的处理，处理失败在将数据递增1（加锁/解锁），否则表示交易成功。
当商品数量递减到0时，表示商品秒杀完毕，拒绝其他用户的请求。



2、这个肯定不能直接操作数据库的，会挂的。直接读库写库对数据库压力太大，要用缓存。
把你要卖出的商品比如10个商品放到缓存中；然后在memcache里设置一个计数器来记录请求数，
这个请求书你可以以你要秒杀卖出的商品数为基数，比如你想卖出10个商品，只允许100个请求进来。
那当计数器达到100的时候，后面进来的就显示秒杀结束，这样可以减轻你的服务器的压力。
然后根据这100个请求，先付款的先得后付款的提示商品以秒杀完。


3、首先，多用户并发修改同一条记录时，肯定是后提交的用户将覆盖掉前者提交的结果了。

这个直接可以使用加锁机制去解决，乐观锁或者悲观锁。
乐观锁，就是在数据库设计一个版本号的字段，每次修改都使其+1，这样在提交时比对提交前的版本号就知道是不是并发提交了，
但是有个缺点就是只能是应用中控制，如果有跨应用修改同一条数据乐观锁就没办法了，这个时候可以考虑悲观锁。
悲观锁，就是直接在数据库层面将数据锁死，类似于oralce中使用select xxxxx from xxxx where xx=xx for update，这样其他线程将无法

4. 用户支付完成发送消息给积分微服务,用户添加积分,同时判断用户的总积分是否够升级会员等级

---
1.从redis中查询商品信息,是否有足够库存可用,减掉redis库存 --- 加redis锁防高并发
2.计算总价生成订单详情
3.发送消息生成订单,订单微服务接收到消息将状态改成排队中,接着发送订单创建事件的消息
4.商品微服务接收到订单创建消息,进行扣减库存,将库存扣减成功与否发送至消息队列
5.订单微服务订阅此消息,若库存扣减成功改变订单状态为已确认,失败则置为取消,取消订单
6.订单微服务通过定时任务商品微服务扣库存失败的情况,进行补偿???没有理解这点
7.发起支付订单时查询订单状态是否已确认

---
还有一种手段就是限流，当既设置了熔断策略，又设置了降级策略，通过全链路的压力测试，应该能够知道整个系统的支撑能力。

因而就需要制定限流策略，保证系统在测试过的支撑能力范围内进行服务，超出支撑能力范围的，可拒绝服务。

当你下单的时候，系统弹出对话框说 “系统忙，请重试”，并不代表系统挂了，而是说明系统是正常工作的，只不过限流策略起到了作用。