package com.chu.cloud.util;

import org.joda.time.format.DateTimeFormat;
import org.springframework.util.Assert;

import java.util.Date;

/**
 * @Description: 订单号生产工具类
 * @author: TianShu.CHU
 * @CreateDate: 2018-05-14 22:41
 * @Version: 1.0
 */
public class OrderNoUtil {

    private static final Integer ORDER_MAX_LENGTH = 24;

    public static synchronized String generateOrderNo(String orderType) {
        Assert.hasText(orderType, "订单流水号前缀不能为空");
        DateTimeFormat.fullDateTime();
        String no = orderType + DateUtils.format(new Date(), DateUtils.YYYYMMDDHHMMSS_SSS);
        if (ORDER_MAX_LENGTH - no.length() > 0)
            no += genRandomNumber(ORDER_MAX_LENGTH - no.length());
        return no;

    }

    private static final String genRandomNumber(int num) {
        java.util.Random rnd = new java.util.Random(System.currentTimeMillis());
        StringBuilder sb = new StringBuilder(num);
        for (int i = 0; i < num; i++)
            sb.append(rnd.nextInt(10));
        return sb.toString();
    }
}
