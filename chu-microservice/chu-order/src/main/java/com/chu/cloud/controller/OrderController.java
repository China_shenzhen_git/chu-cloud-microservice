package com.chu.cloud.controller;

import com.chu.cloud.dto.order.CommonOrderCreateDto;
import com.chu.cloud.dto.order.OrderResult;
import com.chu.cloud.dto.order.SeckillOrderCreateDto;
import com.chu.cloud.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/8/19 14:56
 * @Description:
 */
@RestController
@RequestMapping("/v1/order")
@Slf4j
public class OrderController {

    @Autowired
    private Map<String, OrderService> orderServiceMap;

    @PostMapping("/create/common")
    public OrderResult generateCommonOrder(@RequestBody CommonOrderCreateDto orderCreateDto) {
        OrderService orderService = orderServiceMap.get(orderCreateDto.getOrderType().getValue());
        return orderService.create(orderCreateDto.getMemberId(), orderCreateDto);
    }

    @PostMapping("/create/seckill")
    public OrderResult generateSeckillOrder(@RequestBody SeckillOrderCreateDto seckillOrderDto){
        OrderService orderService = orderServiceMap.get(seckillOrderDto.getOrderType().getValue());
        return orderService.create(seckillOrderDto.getMemberId(), seckillOrderDto);
    }

}
