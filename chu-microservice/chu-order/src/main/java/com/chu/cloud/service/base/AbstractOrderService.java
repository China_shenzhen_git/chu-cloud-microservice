package com.chu.cloud.service.base;

import com.chu.cloud.dto.order.BaseOrderCreateDto;
import com.chu.cloud.dto.order.OrderResult;
import com.chu.cloud.service.OrderService;
import lombok.extern.slf4j.Slf4j;

/**
 * @Description:
 * @author: TianShu.CHU
 * @CreateDate: 2018-08-09 21:57
 * @Version: 1.0
 */
@Slf4j
public abstract class AbstractOrderService extends BaseOrderService implements OrderService {


	@Override
    public final OrderResult create(Integer userId, BaseOrderCreateDto orderCreateDto) {
        return generateOrder(userId,orderCreateDto);
    }

    public abstract OrderResult generateOrder(Integer userId, BaseOrderCreateDto orderCreateDto);



}
