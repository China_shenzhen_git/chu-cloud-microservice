package com.chu.cloud.dto;

import lombok.Data;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/8/10 17:30
 * @Description: 仅供订单生成使用
 */
@Data
public class UserDetail {

    private Integer userId;

    private Integer addressId;

    private String address;

    private String receiveUserName;

    private String receiveUserPhone;


}
