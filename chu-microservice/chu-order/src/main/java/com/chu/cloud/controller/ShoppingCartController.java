package com.chu.cloud.controller;

import com.chu.cloud.service.ShoppingCartService;
import com.chu.cloud.vo.CheckoutResult;
import com.chu.cloud.vo.ShoppingCartResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * @Description:
 * @author: TianShu.CHU
 * @CreateDate: 2018-05-06 16:09
 * @Version: 1.0
 */
@RestController
@RequestMapping("/v1/cart")
public class ShoppingCartController {


    @Autowired
    private ShoppingCartService shoppingCartService;


    @GetMapping("/{memberId}")
    public ShoppingCartResult get(@PathVariable Integer memberId) {

        return shoppingCartService.getShoppingCart(memberId);
    }

    @PostMapping("/checkout/{memberId}")
    public CheckoutResult checkoutCart(@PathVariable Integer memberId) {
        return shoppingCartService.checkoutCart(memberId);
    }


}
