package com.chu.cloud.entity;

import com.chu.cloud.base.AbstractEntity;
import com.chu.cloud.enums.OrderStatusEnums;
import com.chu.cloud.enums.OrderType;
import com.chu.cloud.enums.PayStatusEnums;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/5/9 17:23
 * @Description: 以shopId进行拆分了
 *
 */
@Data
@Entity
@Table(name = "merchant_order",indexes ={@Index(columnList = "userId"),@Index(columnList = "orderNo")} )
public class MerchantOrder extends AbstractEntity implements Serializable {


    private String orderNo;

    private Integer userId;

    private Integer shopId;

    private String address;

    @Enumerated(EnumType.STRING)
    private OrderStatusEnums orderStatus;

    @Enumerated(EnumType.STRING)
    private PayStatusEnums payStatus;

    /**
     * 店铺订单总价
     **/
    private BigDecimal totalPrice;

    /**
     * 店铺支付金额 店铺订单总价 - 店铺优惠券金额
     **/
    private BigDecimal payPrice;

    /**积分订单才能使用**/
    private Integer usePonit;

    /**
     * 获得积分
     **/
    private Integer rewardPoints;

    private String receiveUserName;

    private String receiveUserPhone;

    @Enumerated(EnumType.STRING)
    private OrderType orderType;

    private String payRecordNo;

    private Integer isComment;

    private Date payTime;

    private Date sendTime;

    private Date confirmReceiveTime;

}
