package com.chu.cloud.rabbit.listener;

import com.chu.cloud.rabbit.RabbitConstants;
import com.chu.cloud.repository.MerchantOrderRepository;
import com.chu.cloud.service.OrderService;
import com.chu.cloud.util.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Map;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/6/8 10:44
 * @Description: <p>
 *     订单发起支付会设置回调的url连接,页面js唤起支付,显示success or fail
 *     此时并非真正意义上的成功,微信支付的最终消息会回调这个Url,支付微服务
 *     解析这串xml格式的消息为json体发送到订单微服务
 *     订单微服务接收到此消息,之后不要立马改变订单状态,有可能在传输过程中被恶心篡改,
 *     最终通过查询支付微服务(查询微信支付订单接口)来实现最终一致性
 *
 * </p>
 */
@Slf4j
@Component
@RabbitListener(queues = RabbitConstants.FanoutQueueName.ORDER_PAY_NOTIFY)
public class OrderPaySuccessReceive {

    @Autowired
    private Map<String,OrderService> orderServiceMap;

    @RabbitHandler
    public void receive(String message){
        log.info("接收到消息:{}",message);
        try {
            Map<String, Object> map = JsonUtils.json2map(message);
            String orderNo = (String) map.get("orderNo");
            BigDecimal payPrice = (BigDecimal) map.get("payPrice");
            Integer memberId = (Integer) map.get("memberId");
            String orderType = (String) map.get("orderType");
            orderServiceMap.get(orderType).payNotify(orderNo,payPrice,memberId);
        }catch (Exception e){
            log.info("处理支付订单失败",e);
        }


    }



}
