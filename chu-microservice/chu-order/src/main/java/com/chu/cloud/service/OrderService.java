package com.chu.cloud.service;

import com.chu.cloud.dto.order.BaseOrderCreateDto;
import com.chu.cloud.dto.order.OrderResult;

import java.math.BigDecimal;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/5/9 16:07
 * @Description:
 */
public interface OrderService {


    OrderResult create(Integer userId, BaseOrderCreateDto orderCreateDto);

    /**
     * 生成订单之后30分钟内未支付完成,取消此订单
     * @param orderNo
     */
    void cancelOrder(Integer memberId,String orderNo);


    /**
     *  接收支付微服务消息
     * @param orderNo
     * @param payPrice
     * @param memberId
     */
    void payNotify(String orderNo, BigDecimal payPrice, Integer memberId);



}
