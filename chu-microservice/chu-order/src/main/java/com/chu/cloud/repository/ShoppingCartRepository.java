package com.chu.cloud.repository;

import com.chu.cloud.entity.ShoppingCart;
import com.chu.cloud.enums.CartEventType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @Description:
 * @author: TianShu.CHU
 * @CreateDate: 2018-05-06 16:20
 * @Version: 1.0
 */
public interface ShoppingCartRepository extends JpaRepository<ShoppingCart,Integer>{

    ShoppingCart findByMemberIdAndProductIdAndCartEvent(Integer memberId,Integer productId,CartEventType cartEvent);

    List<ShoppingCart> findByMemberIdAndCartEvent(Integer memberId,CartEventType cartEvent);

    List<ShoppingCart> findByIdIn(List<Integer> ids);
}
