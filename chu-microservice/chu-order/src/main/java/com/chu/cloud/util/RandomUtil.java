package com.chu.cloud.util;

import java.util.Random;

public final class RandomUtil {


    /**
     * 指定位数数字
     *
     * @param charCount 位数
     * @return String rand num
     */
    public static String getRandNum(int charCount) {
        StringBuilder charValue = new StringBuilder();
        for (int i = 0; i < charCount; i++) {
            char c = (char) (randomInt(0, 10) + '0');
            charValue.append(String.valueOf(c));
        }
        return charValue.toString();
    }

    /**
     * 范围内随机数字
     *
     * @param from 开始
     * @param to   结束
     * @return int int
     */
    public static int randomInt(int from, int to) {
        Random r = new Random();
        return from + r.nextInt(to - from);
    }
}