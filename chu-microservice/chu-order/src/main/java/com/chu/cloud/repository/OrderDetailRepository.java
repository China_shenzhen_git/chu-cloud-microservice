package com.chu.cloud.repository;

import com.chu.cloud.entity.OrderDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/5/18 14:36
 * @Description:
 */
@Repository
public interface OrderDetailRepository extends JpaRepository<OrderDetail,Integer>{


    List<OrderDetail> findByOrderNo(String orderNo);

    @Query(value = "select sum(o.quantity) from order_detail o where o.member_id = ?1 and o.product_id = ?2 and o.activity_id = ?3",nativeQuery = true)
    Integer userPurchasedNum(Integer memberId, Integer productId, Integer actId);
}
