package com.chu.cloud.service;

import com.chu.cloud.entity.ShoppingCart;
import com.chu.cloud.vo.CheckoutResult;
import com.chu.cloud.vo.ShoppingCartResult;

/**
 * @Description:
 * @author: TianShu.CHU
 * @CreateDate: 2018-05-06 16:17
 * @Version: 1.0
 */
public interface ShoppingCartService {

    /**
     * 加入购物车,新增事件
     * @param cart
     * @return
     */
    void addCart(ShoppingCart cart);

    /**
     *  就购物车项置为移除事件
     * @param cartId
     * @param memberId
     * @return
     */
    Boolean removeCartItem(Integer cartId, Integer memberId);

    /**
     * 获取用户购物车
     * @param memberId
     * @return
     */
    ShoppingCartResult getShoppingCart(Integer memberId);

    /**
     *  检出购物车
     * @param memberId
     * @return
     */
    CheckoutResult checkoutCart(Integer memberId);

}
