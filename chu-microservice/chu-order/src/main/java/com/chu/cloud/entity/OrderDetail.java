package com.chu.cloud.entity;

import com.chu.cloud.base.AbstractEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/5/18 13:55
 * @Description: 订单商品详情,根据goodsId拆分
 */
@Entity
@Data
@Table(name = "order_detail")
public class OrderDetail extends AbstractEntity implements Serializable{


    private String orderNo;

    private Integer memberId;

    private Integer productId;

    private Integer shopId;

    @Column(name = "quantity",columnDefinition = "int default 0")
    private Integer quantity;

    /**商品价格快照**/
    private BigDecimal productPrice;

    /**支付价格 即商品价格,优惠券不参与退款)**/
    private BigDecimal payPrice;

    /** 0普通商品,1积分商品**/
    private Integer productType;

    private Integer activityId;

    private Integer isReturn;

    private String returnOrderNo;

    private Integer returnStatus;

    private BigDecimal totalReturnPrice;



}
