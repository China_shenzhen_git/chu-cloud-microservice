//package com.chu.cloud.scan;
//
//import com.chu.cloud.util.JsonUtils;
//import org.aspectj.lang.JoinPoint;
//import org.aspectj.lang.ProceedingJoinPoint;
//import org.aspectj.lang.annotation.*;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.stereotype.Component;
//import org.springframework.web.context.request.RequestContextHolder;
//import org.springframework.web.context.request.ServletRequestAttributes;
//
//import javax.servlet.http.HttpServletRequest;
//import java.io.Serializable;
//import java.util.Arrays;
//
///**
// * @Description:AOP日志
// * @author: Tianshu.CHU
// * @date: 2017-12-14 10:46
// */
//@Component
//@Aspect
//public class ShoppingCartLogAspect {
//
//    public static Logger log = LoggerFactory.getLogger(ShoppingCartLogAspect.class);
//
//    private ThreadLocal<Long> startTime = new ThreadLocal<>();
//
//    @Pointcut("execution(public * com.chu.*.controller..*.*(..))")
//    public void cutController() {
//    }
//
//    @Around("cutController()")
//    public void recordSysLog(JoinPoint point) throws Throwable {
//        startTime.set(System.currentTimeMillis());
//        String strMethodName = point.getSignature().getName();
//        String strClassName = point.getTarget().getClass().getName();
//        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
//        HttpServletRequest request = attributes.getRequest();
//
//        //记录请求内容
//        String paramsJson = "unknown params";
//        Object[] paramObjects = point.getArgs();
//        if (paramObjects != null && paramObjects.length > 0) {
//            Object obj = paramObjects[0];
//            if (obj instanceof Serializable) {
//                try {
//                    paramsJson = JsonUtils.obj2json(paramObjects);
//                } catch (Exception e) {
//                    // 忽略无用异常
//                    // e.printStackTrace();
//                }
//            }
//        } else {
//            paramsJson = "";
//        }
////        log.info("ip:{},request url:{},params:{}", request.getRemoteAddr(), request.getRequestURI().toString(), paramsJson);
//
//        String url = request.getRequestURL().toString();
//        String strMessage = String
//                .format("----->[请求URL]:%s,[类名]:%s,[方法]:%s,[参数]:%s", url, strClassName, strMethodName, paramsJson);
//        log.info(strMessage);
//
//    }
//
//    @AfterReturning("cutController()")
//    public void doAfterReturning(JoinPoint joinPoint) {
//        long takeTimes = System.currentTimeMillis() - startTime.get();
//        if(takeTimes >= 100) {
//            // 处理完请求，返回内容
//            log.info("响应慢请求耗时（毫秒）:{}", takeTimes);
//        }
//        startTime.remove();
//    }
//
//}
