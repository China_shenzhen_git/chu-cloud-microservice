//package com.chu.cloud.entity;
//
//import com.chu.cloud.base.AbstractEntity;
//import com.chu.cloud.enums.OrderStatusEnums;
//import com.chu.cloud.enums.PayStatusEnums;
//import lombok.Data;
//
//import javax.persistence.*;
//import java.io.Serializable;
//import java.math.BigDecimal;
//import java.util.Date;
//
///**
// * @author: Tianshu.CHU
// * @Date: 2018/8/29 14:48
// * @Description: 订单主表 --->这个版本暂时不做
// */
////@Entity
//@Data
//public class OrderMaster extends AbstractEntity implements Serializable{
//
////    @Id
////    @GeneratedValue
//    private Long id;
//
//    private String orderNo;
//
//    private Integer memberId;
//
////    @Enumerated(EnumType.STRING)
//    private OrderStatusEnums orderStatus;
//
////    @Enumerated(EnumType.STRING)
//    private PayStatusEnums payStatus;
//
//    private BigDecimal orderPrice;
//
//    private BigDecimal orderPayPrice;
//
//    private BigDecimal orderCouponPrice;
//
//    private Date createTime;
//
//    private Date updateTime;
//}
