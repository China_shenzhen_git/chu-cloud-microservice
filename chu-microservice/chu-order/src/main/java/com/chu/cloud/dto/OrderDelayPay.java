package com.chu.cloud.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/8/30 10:42
 * @Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderDelayPay implements Serializable{

    private String orderNo;

    private Integer userId;

    private String orderType;
}
