package com.chu.cloud.entity;

import com.chu.cloud.base.AbstractEntity;
import com.chu.cloud.enums.CartEventType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.io.Serializable;

/**
 * @Description:
 * @author: TianShu.CHU
 * @CreateDate: 2018-05-06 15:55
 * @Version: 1.0
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ShoppingCart extends AbstractEntity implements Serializable {


    private Integer productId;

    private Integer memberId;

    private Integer shopId;

    private Integer num;

    @Enumerated(EnumType.STRING)
    @Column(name = "cart_event")
    private CartEventType cartEvent;


}
