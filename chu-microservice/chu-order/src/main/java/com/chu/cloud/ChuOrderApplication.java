package com.chu.cloud;

import com.chu.cloud.stream.channel.OrderPaySuccessProcessor;
import com.chu.cloud.stream.channel.OrderProductReturnProcessor;
import com.chu.cloud.stream.channel.SeckillExecuteProcessor;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.web.client.RestTemplate;

@EnableFeignClients(basePackages = "com.chu.cloud.feign.client")
@SpringCloudApplication
@EnableBinding({OrderProductReturnProcessor.class, SeckillExecuteProcessor.class, OrderPaySuccessProcessor.class})
@EnableJpaAuditing
@EnableHystrix
public class ChuOrderApplication {


	@Bean
	@LoadBalanced
	public RestTemplate restTemplate(){
		return new RestTemplate();
	}

	public static void main(String[] args) {
		SpringApplication.run(ChuOrderApplication.class,args);
	}

}
