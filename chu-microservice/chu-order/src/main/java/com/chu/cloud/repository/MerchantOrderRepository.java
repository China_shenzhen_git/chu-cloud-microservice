package com.chu.cloud.repository;

import com.chu.cloud.entity.MerchantOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/5/18 11:25
 * @Description:
 */
@Repository
public interface MerchantOrderRepository extends JpaRepository<MerchantOrder,Integer>{


    MerchantOrder findByOrderNo(String orderNo);


    MerchantOrder findByUserIdAndOrderNo(Integer memberId,String orderNo);


}
