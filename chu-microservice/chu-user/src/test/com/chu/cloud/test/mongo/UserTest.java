package com.chu.cloud.test.mongo;

import com.chu.cloud.ChuSsoApplicationTest;
import com.chu.cloud.entity.User;
import com.chu.cloud.repository.UserRepository;
import com.chu.cloud.service.impl.UserService;
import com.chu.cloud.util.BeanUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @Description:
 * @author: Tianshu.CHU
 * @date: 2018-03-21 17:35
 */
public class UserTest extends ChuSsoApplicationTest {


    @Autowired
    private UserService userService;


    @Test
    public void loginByMobile() throws Exception {
        User newUser = new User();
        newUser.setMobile("abc");
        User update = userService.update(1, newUser);
        System.out.println(update);

    }


}
