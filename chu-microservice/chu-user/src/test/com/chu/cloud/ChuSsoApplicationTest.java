package com.chu.cloud;



import com.chu.cloud.entity.Address;
import com.chu.cloud.entity.User;
import com.chu.cloud.enums.UserGradeEnum;
import com.chu.cloud.repository.AddressRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


/**
 * @Description:
 * @author: Tianshu.CHU
 * @date: 2018-03-21 17:35
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class ChuSsoApplicationTest {

    @Autowired
    private AddressRepository addressRepository;

    @Test
    public void test(){
        Address address = new Address();
        address.setCityId(1);
        addressRepository.save(address);

    }

}
