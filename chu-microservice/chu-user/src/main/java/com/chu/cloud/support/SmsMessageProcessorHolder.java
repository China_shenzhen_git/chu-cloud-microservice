package com.chu.cloud.support;

import com.chu.cloud.enums.sms.SmsCodeType;
import com.chu.cloud.util.ChuException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/9/20 14:54
 * @Description:
 */
@Component
public class SmsMessageProcessorHolder {

    @Autowired
    private Map<String,SmsMessageProcessor> messageProcessors;


    public SmsMessageProcessor findMessageProcessor(SmsCodeType smsCodeType){
        String name = smsCodeType.getType() + SmsMessageProcessor.class.getSimpleName();
        SmsMessageProcessor processor = messageProcessors.get(name);
        if (processor == null){
            throw new ChuException("发送短信处理器"+name+"不存在");
        }
        return processor;
    }

}
