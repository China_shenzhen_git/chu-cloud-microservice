package com.chu.cloud.config;

import com.chu.cloud.entity.Address;
import com.chu.cloud.entity.MemberCoupon;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;

/**
 * @Description：为了解决Spring Data Rest不暴露ID字段的问题。
 * 参考：http://tommyziegler.com/how-to-expose-the-resourceid-with-spring-data-rest/
 */
@Configuration
public class SpringDataRestConfig {
    @Bean
    public RepositoryRestConfigurer repositoryRestConfigurer() {
        return new RepositoryRestConfigurerAdapter() {
            @Override
            public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
                config.exposeIdsFor(Address.class, MemberCoupon.class);
            }
        };
    }
}