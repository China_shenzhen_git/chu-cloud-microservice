package com.chu.cloud.controller;

import com.chu.cloud.entity.MemberCoupon;
import com.chu.cloud.repository.MemberCouponRepository;
import com.chu.cloud.service.impl.UserService;
import com.chu.cloud.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @Description:
 * @author: Tianshu.CHU
 * @date: 2018-03-21 14:35
 */
@RestController
@RequestMapping("/v1/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private MemberCouponRepository couponRepository;

    @PostMapping(value = "/login/{mobile}/{validateCode}/{ip}")
    public String login(@PathVariable String mobile, @PathVariable String validateCode, @PathVariable String ip) {
        String token = userService.loginByMobile(mobile, validateCode, ip);
        return token;
    }

    @GetMapping("/smsCode/{mobile}")
    public String createCode(@PathVariable String mobile) {
        return "-1";
    }

    @GetMapping("/find/{token}")
    public UserVo findByToken(@PathVariable String token) {
        return userService.findByToken(token);
    }


    @PostMapping("/coupons")
    public List<MemberCoupon> findByIdIn(@RequestBody List<Integer> ids){
        return couponRepository.findByIdIn(ids);
    }
}
