package com.chu.cloud.repository;

import com.chu.cloud.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * @Description:
 * @author: Tianshu.CHU
 * @date: 2018-03-21 17:24
 */
@Repository
public interface UserRepository extends JpaRepository<User,Integer> {

    /**
     * 根据手机号查询用户
     * @param mobile
     * @return
     */
    User findByMobile(String mobile);

    @Modifying
    @Query(value = "update user u set u.mobile = ? where u.id = ?",nativeQuery = true)
    @Transactional
    void update(String mobile,Integer id);

}
