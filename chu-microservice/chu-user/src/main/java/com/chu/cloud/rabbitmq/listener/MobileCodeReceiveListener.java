package com.chu.cloud.rabbitmq.listener;

import com.chu.cloud.support.SmsMessageProcessorHolder;
import com.chu.cloud.rabbit.RabbitConstants;
import com.chu.cloud.dto.MobileSmsCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/3/29 15:18
 * @Description: 监听短信发送请求
 */
@Slf4j
@Component
@RabbitListener(queues = RabbitConstants.DirectQueueName.MOBILE_CODE_QUEUE)
public class MobileCodeReceiveListener {


    @Autowired
    private SmsMessageProcessorHolder messageProcessorHolder;

    @RabbitHandler
    public void receive(MobileSmsCode smsCode){
        try{
            log.info("接收到消息={}",smsCode);
            messageProcessorHolder.findMessageProcessor(smsCode.getType()).process(smsCode);
        }catch (Exception e){
            log.error("消费队列{}出现异常信息={}",RabbitConstants.DirectQueueName.MOBILE_CODE_QUEUE,e);
        }
    }



}
