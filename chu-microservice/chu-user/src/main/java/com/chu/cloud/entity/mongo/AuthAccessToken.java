package com.chu.cloud.entity.mongo;

import com.chu.cloud.base.BaseEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
public class AuthAccessToken extends BaseEntity<String> {

    private static final long serialVersionUID = 3994610346750664862L;

    private Integer userId;

    private String mobile;

    private String userName;

    private String bindIp;

    private String token;

    /**token是否有效*/
    private Boolean status;

    /** token过期时间 */
    private Date expireTime;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getBindIp() {
        return bindIp;
    }

    public void setBindIp(String bindIp) {
        this.bindIp = bindIp;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Date getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Date expireTime) {
        this.expireTime = expireTime;
    }
}
