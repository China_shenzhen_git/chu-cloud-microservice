package com.chu.cloud.entity;

import com.chu.cloud.base.AbstractEntity;
import com.chu.cloud.enums.UserGradeEnum;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.io.Serializable;
import java.util.Date;

/**
 * @Description:
 * @author: Tianshu.CHU
 * @date: 2018-03-22 15:57
 */
@Entity
@Data
public class User extends AbstractEntity implements Serializable {

    private String userName;

    private String mobile;

    @Enumerated(EnumType.STRING)
    private UserGradeEnum grade;

    private Integer status;

    private Integer loginNum;

    private Date registerTime;

    private String registerIp;


}
