package com.chu.cloud.rabbitmq.listener;

import com.chu.cloud.rabbit.RabbitConstants;
import com.chu.cloud.service.MemberCouponService;
import com.chu.cloud.stream.payload.OrderCouponUseLoad;
import com.chu.cloud.util.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/5/21 11:54
 * @Description:
 */
@Slf4j
@Component
@RabbitListener(queues = RabbitConstants.DirectQueueName.ORDER_COUPON_USE)
public class OrderCouponUseReceiveListener {

    @Autowired
    private MemberCouponService memberCouponService;

    @RabbitHandler
    public void receive(OrderCouponUseLoad couponUseLoad) {
        log.info("收到来自:{}的消息:{}", RabbitConstants.DirectQueueName.ORDER_COUPON_USE,
                JsonUtils.obj2json(couponUseLoad));
        CompletableFuture.runAsync(() -> {
            memberCouponService.useCoupon(couponUseLoad);
        }).exceptionally(e ->{
            log.error("异常信息",e.getMessage());
            return null;
        });
    }


}
