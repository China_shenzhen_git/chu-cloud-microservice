package com.chu.cloud.entity;

import com.chu.cloud.base.AbstractEntity;
import com.chu.cloud.enums.PointActionEnums;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/6/8 15:47
 * @Description: 用户订单积分记录 :
 *              <p>
 *                   1.从某一订单获取积分
 *                   2.从某一订单消费积分
 *              </p>
 *
 */
@Entity
@Data
public class UserPoint extends AbstractEntity implements Serializable{

    private Integer memberId;

    private Integer point;

    @Column(name = "order_id")
    private String lockForOrderId;

    private PointActionEnums action;

    @Version
    private Long version;

}
