package com.chu.cloud.controller;

import com.chu.cloud.entity.Address;
import com.chu.cloud.service.impl.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/4/16 14:21
 * @Description: 用户收货地址
 */
@RestController
@RequestMapping("/v1/address")
public class AddressController {

    @Autowired
    private AddressService addressService;


    @PostMapping("/add")
    public Integer addAddress(@RequestBody Address address) {
        return addressService.save(address);
    }

    @GetMapping("/{id}")
    public Address findByAddressId(@PathVariable Integer id) {
        return addressService.findAddressId(id);
    }

    @GetMapping("/default/{userId}")
    public Address findDefault(@PathVariable Integer userId) {
        return addressService.findDefault(userId);
    }

}
