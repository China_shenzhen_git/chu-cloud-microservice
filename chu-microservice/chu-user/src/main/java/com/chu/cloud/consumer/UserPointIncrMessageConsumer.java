package com.chu.cloud.consumer;

import com.chu.cloud.service.impl.PointService;
import com.chu.cloud.stream.channel.OrderPaySuccessProcessor;
import com.chu.cloud.stream.consumer.MessageConsumer;
import com.chu.cloud.stream.payload.OrderPaySuccessDto;
import com.chu.cloud.util.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;


/**
 *  用户积分增加监听者
 * @author: Tianshu.CHU
 * @Date: 2018/9/4 11:47
 */
@Component
@Slf4j
public class UserPointIncrMessageConsumer implements MessageConsumer<OrderPaySuccessDto> {


    @Autowired
    private PointService pointService;

    @StreamListener(OrderPaySuccessProcessor.USER_POINT)
    @Override
    public void recevieMessage(OrderPaySuccessDto payLoad) {
        log.info("用户微服务收到消息:{}", JsonUtils.obj2json(payLoad));
        CompletableFuture.runAsync(() ->{
            pointService.rewardPoint(payLoad);
        }).exceptionally(e ->{
            log.error("发生异常",e.getMessage());
            return null;
        });

    }
}
