package com.chu.cloud.entity;

import com.chu.cloud.base.AbstractEntity;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/4/16 14:37
 * @Description:
 */
@Entity
@Data
public class Address extends AbstractEntity implements Serializable{

    private Integer userId;

    private String consignee;

    private String mobile;

    private Integer provinceId;

    private Integer cityId;

    private Integer regionId;

    private String address;

    private boolean defaultAddress;

    private boolean isDelete;

}
