package com.chu.cloud.support;

import com.chu.cloud.dto.MobileSmsCode;
import com.chu.cloud.enums.RedisKeyPrefix;
import com.chu.cloud.util.RedisUtil;
import org.springframework.util.Assert;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/3/29 17:30
 * @Description: 抽象Handler
 */
public abstract class AbstractMessageProcessor implements SmsMessageProcessor {


    /**
     * 业务处理
     *
     * @param smsCode 信息
     */
    @Override
    public void process(MobileSmsCode smsCode) {
        Assert.notNull(smsCode.getMobile(), "手机不能为空");
        send(smsCode);
        save(smsCode.getMobile(), smsCode.getCode());
    }

    private void save(String mobile, String code) {
        RedisUtil.set(RedisKeyPrefix.MOBILE_SMSCODE, mobile, code, 5 * 60);
    }

    /**
     * 发送短信,由子类决定
     *
     * @param smsCode
     */
    protected abstract void send(MobileSmsCode smsCode);


}
