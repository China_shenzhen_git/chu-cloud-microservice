package com.chu.cloud.entity;

import com.chu.cloud.base.AbstractEntity;
import com.chu.cloud.enums.CouponEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Entity
@ApiModel(value = "member_coupon")
@Data
public class MemberCoupon extends AbstractEntity implements Serializable {


    private static final long serialVersionUID = -9117584160338011543L;


    @ApiModelProperty(value = "会员ID", required = true)
    @NotNull
    private Integer memberId;

    @ApiModelProperty(value = "店铺ID（0 默认平台））", required = true)
    @NotNull
    @Column(name = "shop_id", length = 10)
    private Integer shopId;


    @ApiModelProperty(value = "优惠劵名称")
    @Column(name = "coupon_discount_name", length = 100)
    private String couponDiscountName;


    @ApiModelProperty(value = "订单号")
    @Column(name = "order_no", length = 100)
    private String orderNo;


    @ApiModelProperty(value = "劵状态：GET：领取 USED：已使用 EXPIRED：过期")
    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private CouponEnum status;


    private Date useStartDate;

    @ApiModelProperty(value = "过期时间")
    @Column(name = "expiration_date", length = 19)
    private Date expirationDate;



    @ApiModelProperty(value = "优惠卷劵ID")
    @Column(name = "coupon_discount_id", length = 19)
    private Integer couponDiscountId;

    private Integer couponNum;

    @Version
    private Integer version;
}