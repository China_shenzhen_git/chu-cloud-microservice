package com.chu.cloud.support;

import com.chu.cloud.dto.MobileSmsCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/3/29 17:40
 * @Description: 阿里云短信发送类
 */
@Component("aliyunSmsMessageProcessor")
@Slf4j
public class AliyunSmsMessageProcessor extends AbstractMessageProcessor {


    @Override
    protected void send(MobileSmsCode smsCode) {
        //TODO 调用阿里大鱼发送短信
        log.info("=======调用了阿里大鱼发送短信成功====");
    }
}
