package com.chu.cloud.repository;

import com.chu.cloud.entity.mongo.AuthAccessToken;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/3/28 10:23
 * @Description:
 */
@Repository
public interface AuthAccessTokenDao extends MongoRepository<AuthAccessToken,String>{


    AuthAccessToken findByToken(String token);
}
