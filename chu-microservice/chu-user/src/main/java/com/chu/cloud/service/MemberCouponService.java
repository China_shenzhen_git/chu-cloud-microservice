package com.chu.cloud.service;

import com.chu.cloud.dto.MemberGetCouponDto;
import com.chu.cloud.stream.payload.OrderCouponUseLoad;

public interface MemberCouponService {

    /**
     * 保存用户领取的优惠券
     * @param couponDto
     */
    void saveUserCoupon(MemberGetCouponDto couponDto);

    /**
     *  用户订单使用优惠券
     * @param payLoad
     */
    void useCoupon(OrderCouponUseLoad payLoad);
}
