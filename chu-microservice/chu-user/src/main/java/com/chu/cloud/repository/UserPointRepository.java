package com.chu.cloud.repository;

import com.chu.cloud.entity.UserPoint;
import com.chu.cloud.enums.PointActionEnums;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/6/8 15:50
 * @Description:
 */
public interface UserPointRepository extends JpaRepository<UserPoint,Integer>{

    @Query(value = "select sum(p.point) from user_point where p.member_id = ? and p.action=?",nativeQuery = true)
    Integer findPoint(Integer userId, PointActionEnums actionEnums);
}
