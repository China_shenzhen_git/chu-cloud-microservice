package com.chu.cloud.repository;

import com.chu.cloud.entity.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/4/16 15:16
 * @Description:
 */
@RepositoryRestResource(path = "address")
public interface AddressRepository extends JpaRepository<Address,Integer>{


    @Modifying
    @Query(value = "update address a set a.default_address = 0 where a.user_id = ?",nativeQuery = true)
    void clearDefaultAddress(@Param("userId") Integer userId);


    Address findByUserIdAndDefaultAddress(Integer userId,boolean isDefault);

}
