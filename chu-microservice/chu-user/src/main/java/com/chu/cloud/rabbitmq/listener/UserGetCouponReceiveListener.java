package com.chu.cloud.rabbitmq.listener;

import com.chu.cloud.dto.MemberGetCouponDto;
import com.chu.cloud.rabbit.RabbitConstants;
import com.chu.cloud.service.MemberCouponService;
import com.chu.cloud.util.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;

/**
 * @Description: 监听用户领取优惠券请求
 * @author: TianShu.CHU
 * @CreateDate: 2018-05-03 22:56
 * @Version: 1.0
 */
@Slf4j
@Component
@RabbitListener(queues = RabbitConstants.DirectQueueName.USER_GET_COUPON)
public class UserGetCouponReceiveListener {

    @Autowired
    private MemberCouponService memberCouponService;

    @RabbitHandler
    public void receive(MemberGetCouponDto payLoad) {
        log.info("接收到消息{}", JsonUtils.obj2json(payLoad));
        CompletableFuture.runAsync(() -> {
            memberCouponService.saveUserCoupon(payLoad);
        }).exceptionally(e -> {
            log.error("异常信息", e.getMessage());
            return null;
        });
    }

}
