package com.chu.cloud.support;

import com.chu.cloud.dto.MobileSmsCode;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/3/29 17:28
 * @Description: 发送短信接口
 */
public interface SmsMessageProcessor {


    void process(MobileSmsCode smsCode);


}
