package com.chu.cloud.repository;

import com.chu.cloud.entity.MemberCoupon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * @Description:
 * @author: TianShu.CHU
 * @CreateDate: 2018-05-03 23:04
 * @Version: 1.0
 */
@RepositoryRestResource(path = "memberCoupon")
public interface MemberCouponRepository extends JpaRepository<MemberCoupon,Integer>{

    List<MemberCoupon> findByMemberIdAndCouponDiscountId(Integer memberIdm,Integer couponId);


    MemberCoupon[] findByMemberIdAndShopId(@Param("memberId") Integer memberId, @Param("shopId") Integer shopId);


    List<MemberCoupon> findByIdIn(List<Integer> ids);
}
