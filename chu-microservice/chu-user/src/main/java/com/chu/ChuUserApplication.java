package com.chu;

import com.chu.cloud.stream.channel.CouponNumReduceProcessor;
import com.chu.cloud.stream.channel.OrderPaySuccessProcessor;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringCloudApplication
@EnableJpaRepositories("com.**")
@EnableJpaAuditing
@EnableWebMvc
@EnableBinding({CouponNumReduceProcessor.class, OrderPaySuccessProcessor.class})
@EnableFeignClients
@EnableHystrix
public class ChuUserApplication {


	@LoadBalanced
	@Bean
	public RestTemplate restTemplate(){
		return new RestTemplate();
	}

	public static void main(String[] args) {
		SpringApplication.run(ChuUserApplication.class, args);
	}




}
