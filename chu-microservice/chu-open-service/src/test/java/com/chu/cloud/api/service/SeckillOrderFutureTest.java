package com.chu.cloud.api.service;

import com.chu.cloud.enums.RedisKeyPrefix;
import com.chu.cloud.util.RedisUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/9/11 14:05
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class SeckillOrderFutureTest {


    @Test
    public void test(){
        String key = "1:1";
        Integer stock = RedisUtil.get(RedisKeyPrefix.SECKILL_STOCKS, key, Integer.class);
        System.out.println(stock);
    }

}