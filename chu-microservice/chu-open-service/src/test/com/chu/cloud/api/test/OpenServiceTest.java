package com.chu.cloud.api.test;

import com.chu.cloud.enums.RedisKeyPrefix;
import com.chu.cloud.util.RedisUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/9/11 16:52
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class OpenServiceTest {


    @Test
    public void test(){
        Integer strock = RedisUtil.get(RedisKeyPrefix.SECKILL_STOCKS, "1:2", Integer.class);
        System.out.println(strock);
    }
}
