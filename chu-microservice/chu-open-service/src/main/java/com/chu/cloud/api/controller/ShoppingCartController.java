package com.chu.cloud.api.controller;

import com.chu.cloud.api.service.ShoppingCartFuture;
import com.chu.cloud.api.thread.MemberContextLocal;
import com.chu.cloud.response.ResultVo;
import com.chu.cloud.vo.CheckoutResult;
import com.chu.cloud.vo.ShoppingCartResult;
import com.chu.cloud.vo.ShoppingCartVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.CompletableFuture;

/**
 * @Description:
 * @author: TianShu.CHU
 * @CreateDate: 2018-05-06 16:25
 * @Version: 1.0
 */
@RestController
@RequestMapping("/carts")
@Api(value = "购物车相关api")
public class ShoppingCartController {

    @Autowired
    private ShoppingCartFuture shoppingCartFuture;

    @ApiOperation(value = "加入购物车")
    @PostMapping("/add")
    public CompletableFuture<ResultVo<String>> addCart(@RequestBody ShoppingCartVo cartVo) {
        Integer memberId = MemberContextLocal.get().getId();
        cartVo.setMemberId(memberId);
        return shoppingCartFuture.addCart(cartVo).thenApply(ResultVo::ok);
    }

    @ApiOperation(value = "移除购物车项")
    @PostMapping("/remove/{cartId:\\d+}")
    public CompletableFuture<ResultVo<Boolean>> removeCartItem(@PathVariable Integer cartId) {
        Integer memberId = MemberContextLocal.get().getId();
        return shoppingCartFuture.removeCartItem(cartId, memberId).thenApply(result -> {
            return ResultVo.ok(result);
        });
    }

    @GetMapping("/get")
    public CompletableFuture<ResultVo<ShoppingCartResult>> getUserCart() {

        return shoppingCartFuture.getUserCart(MemberContextLocal.get().getId());
    }

    @PostMapping("/checkout")
    public CompletableFuture<ResultVo<CheckoutResult>> checkout() {
        Integer memberId = MemberContextLocal.get().getId();
        return shoppingCartFuture.checkoutCart(memberId).thenApply(ResultVo::ok);

    }

}
