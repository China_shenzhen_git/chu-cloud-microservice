package com.chu.cloud.api.thread;

import com.chu.cloud.vo.UserVo;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/4/13 11:09
 * @Description:  <p> 利用ThreadLocal类做线程隔离,此类中维护一个Map,以当前线程为key
 *              配合拦截器封装当前用户,一次请求有效
 */
public final class MemberContextLocal {


    private static final ThreadLocal<UserVo> LOCAL = new ThreadLocal<>();

    private static final MemberContextLocal  MEMBER_CONTEXT_LOCAL = new MemberContextLocal();


    public static MemberContextLocal getInstance(){
        return MEMBER_CONTEXT_LOCAL;
    }

    /**
     * 获取
     * @return
     */
    public static UserVo get(){
        return LOCAL.get();
    }

    /**
     *  赋值
     * @param user
     */
    public static void set(final UserVo user){
        LOCAL.set(user);
    }

    /**
     * 移除
     */
    public static void remove(){
        LOCAL.remove();
    }
}
