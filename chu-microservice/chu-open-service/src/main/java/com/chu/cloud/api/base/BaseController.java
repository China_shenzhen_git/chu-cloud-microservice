package com.chu.cloud.api.base;

import com.chu.cloud.util.JsonUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Description:
 * @author: TianShu.CHU
 * @CreateDate: 2018-04-09 20:23
 * @Version: 1.0
 */
public abstract class BaseController {


    /**
     * 将前台传递过来的日期格式的字符串，自动转化为Date类型
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }


    /**
     * 获取请求ip
     *
     * @param req
     * @return
     */
    public static String getIp(HttpServletRequest req) {

        String ip = req.getHeader("x-forwarded-for") == null ? req.getHeader("X-Forwarded-For")
                : req.getHeader("x-forwarded-for");
        if (StringUtils.isBlank(ip)) {
            ip = req.getHeader("Proxy-Client-IP");
        }
        if (StringUtils.isBlank(ip)) {
            ip = req.getHeader("WL-Proxy-Client-IP");
        }
        if (StringUtils.isBlank(ip) || "0:0:0:0:0:0:0:1".equals(ip) || "unknown".equalsIgnoreCase(ip)) {
            ip = req.getRemoteAddr();
        }
        return ip;
    }

    /**
     * 向客户端返回json或者jsonp数据，支持ajax跨域请求
     * @param result
     * @param req
     * @param res
     */
    public static void responseJson(Object result, HttpServletRequest req, HttpServletResponse res) {
        try {
            if (result != null) {
                res.setHeader("Pragma", "No-cache");
                res.setHeader("Cache-Control", "no-cache");
                res.setDateHeader("Expires", 0);
                res.setContentType("text/json");
                res.setCharacterEncoding("UTF-8");

                PrintWriter out = res.getWriter();
                String callback = req.getParameter("callback");
                String json = "";
                if (result instanceof String) {
                    json = (String) result;
                } else {
                    json = JsonUtils.obj2json(result);
                }
                // 是否为跨域请求，如果是，则拼接jsonp数据
                if (org.apache.commons.lang3.StringUtils.isNotBlank(callback)) {
                    json = callback + "(" + json + ")";
                }
                out.write(json);
                out.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static String getToken(HttpServletRequest req) {

        return req.getHeader("header");

    }
}
