package com.chu.cloud;

import com.chu.cloud.stream.channel.OrderPaySuccessProcessor;
import com.chu.cloud.stream.channel.SeckillExecuteProcessor;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableFeignClients(basePackages = "com.chu.cloud.feign.client")
@EnableBinding({SeckillExecuteProcessor.class, OrderPaySuccessProcessor.class})
@EnableWebMvc
@SpringCloudApplication
@EnableHystrix
public class ChuOpenServiceApplication {

	@LoadBalanced
	@Bean
	public RestTemplate restTemplate(){
		return new RestTemplate();
	}


	public static void main(String[] args) {
		SpringApplication.run(ChuOpenServiceApplication.class, args);
	}
}
