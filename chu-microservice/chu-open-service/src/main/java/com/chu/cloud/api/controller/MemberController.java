package com.chu.cloud.api.controller;

import com.chu.cloud.api.base.BaseController;
import com.chu.cloud.api.service.MemberService;
import com.chu.cloud.enums.sms.SmsMessageType;
import com.chu.cloud.feign.client.IUserFeignClient;
import com.chu.cloud.response.ResultVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/4/11 10:31
 * @Description:
 */
@RestController
@RequestMapping("/member")
@Api(value = "用户登录注册相关API")
@Slf4j
public class MemberController extends BaseController{

    @Value("${isDev:true}")
    private Boolean isDev;

    @Autowired
    private MemberService memberService;

    @Autowired
    private IUserFeignClient userFeignClient;

    @GetMapping("/smsCode")
    @ApiOperation(value = "获取登陆验证码", httpMethod = "GET")
    public ResponseEntity<String> getLoginSmsCode(@RequestParam String mobile) {
        String result = memberService.sendSmsCode(mobile, SmsMessageType.LOGIN);
        if (result == null) {
            return new ResponseEntity<String>("验证码未失效,可以继续使用!", HttpStatus.OK);
        }
        if (isDev) {
            return ResponseEntity.ok(result);
        }
        return ResponseEntity.ok("发送成功");
    }

    @ApiOperation(value = "手机验证码登陆")
    @PostMapping("/login/{mobile}/{validateCode}")
    public ResultVo<String> loginByMobile(@PathVariable String mobile,
                                          @PathVariable String validateCode, HttpServletRequest request) {
        Assert.notNull(mobile, "手机号不能为空");
        Assert.notNull(validateCode, "验证码不能为空");
        String ip = getIp(request);
        return memberService.loginByMobile(mobile, validateCode, ip);

    }

}
