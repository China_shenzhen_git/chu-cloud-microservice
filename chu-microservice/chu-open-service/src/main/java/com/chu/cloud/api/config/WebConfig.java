package com.chu.cloud.api.config;

import com.chu.cloud.api.handler.AuthInterceptor;
import com.chu.cloud.api.properties.AuthUrlsProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/4/13 16:48
 * @Description:
 */
@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {

    @Autowired
    private AuthUrlsProperties authUrlsProperties;

    @Autowired
    private AuthInterceptor authInterceptor;


    public String[] getAuthUrl() {
//        List<String> authUrls = authUrlsProperties.getIgnoreUrls();
        String[] urls = new String[]{
                "/coupons/get/**",
                "/carts/**",
                "/orders/**"
        };
        return urls;

    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(authInterceptor).addPathPatterns(getAuthUrl());
        super.addInterceptors(registry);
    }

}
