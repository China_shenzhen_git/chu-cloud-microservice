package com.chu.cloud.api.service;

import com.chu.cloud.enums.CartEventType;
import com.chu.cloud.feign.client.IShoppingCartFeignClient;
import com.chu.cloud.rabbit.RabbitConstants;
import com.chu.cloud.rabbit.fanout.send.RabbitFanoutSender;
import com.chu.cloud.response.ResponseMessage;
import com.chu.cloud.response.ResultVo;
import com.chu.cloud.vo.CheckoutResult;
import com.chu.cloud.vo.ProductVo;
import com.chu.cloud.vo.ShoppingCartResult;
import com.chu.cloud.vo.ShoppingCartVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CompletableFuture;

/**
 * @Description:
 * @author: TianShu.CHU
 * @CreateDate: 2018-05-06 16:26
 * @Version: 1.0
 */
@Service
public class ShoppingCartFuture {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private IShoppingCartFeignClient shoppingCartFeignClient;

    @Autowired
    private RabbitFanoutSender fanoutSender;


    public CompletableFuture<String> addCart(ShoppingCartVo cartVo) {
        return CompletableFuture.supplyAsync(() -> {
            String url = String.format("http://chu-product/products/%s", cartVo.getProductId());
            ResponseEntity<ProductVo> responseEntity = restTemplate.getForEntity(url, ProductVo.class);
            ProductVo productVo = responseEntity.getBody();
            if (productVo.getStocks().compareTo(cartVo.getNum()) < 0) {
                return "超出最大数量";
            }
            //允许加入加购物车
            cartVo.setCartEvent(CartEventType.ADD_ITEM);
            fanoutSender.sendFanoutMessage(RabbitConstants.FanoutExchangeName.SHOPPING_CART_EVENT, cartVo);
            return "添加成功";

        });
    }

    public CompletableFuture<Boolean> removeCartItem(Integer cartId, Integer memberId) {
        return CompletableFuture.supplyAsync(() -> {
            ShoppingCartVo cartVo = new ShoppingCartVo();
            cartVo.setId(cartId);
            cartVo.setMemberId(memberId);
            cartVo.setCartEvent(CartEventType.REMOVE_ITEM);
            fanoutSender.sendFanoutMessage(RabbitConstants.FanoutExchangeName.SHOPPING_CART_EVENT, cartVo);
            return true;
        });
    }

    public CompletableFuture<ResultVo<ShoppingCartResult>> getUserCart(Integer memberId) {
        return CompletableFuture.supplyAsync(() ->{
            ResponseMessage<ShoppingCartResult> cartResult = shoppingCartFeignClient.getUserCart(memberId);
            if (cartResult.getCode() != 200) {
                return ResultVo.getError(cartResult.getCode(), cartResult.getMessage());
            }
            return ResultVo.ok(cartResult.getData());
        });
    }

    public CompletableFuture<CheckoutResult> checkoutCart(Integer memberId) {
        return CompletableFuture.supplyAsync(() -> {
            ResponseMessage<CheckoutResult> checkoutResult = shoppingCartFeignClient.checkoutCart(memberId);
            return checkoutResult.getData();
        });
    }
}
