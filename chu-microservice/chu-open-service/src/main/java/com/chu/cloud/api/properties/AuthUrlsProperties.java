package com.chu.cloud.api.properties;

import lombok.Data;
import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/4/13 14:46
 * @Description:拦截的url
 */
@Component
@ConfigurationProperties("auth")
@Data
public class AuthUrlsProperties {

    private List<String> ignoreUrls;

}
