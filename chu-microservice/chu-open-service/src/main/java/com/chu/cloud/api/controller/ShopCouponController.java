package com.chu.cloud.api.controller;

import com.chu.cloud.api.base.BaseController;
import com.chu.cloud.api.service.ShopCouponService;
import com.chu.cloud.api.thread.MemberContextLocal;
import com.chu.cloud.dto.CouponsDto;
import com.chu.cloud.response.ResultVo;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/5/3 18:59
 * @Description:
 */
@RestController
@RequestMapping("/coupons")
public class ShopCouponController extends BaseController{

    @Autowired
    private ShopCouponService shopCouponService;

    @GetMapping("/list/{shopId}")
    @ApiOperation(value = "店铺当前时间可领取的优惠券列表")
    public ResultVo<List<CouponsDto>> couponLists(@PathVariable Integer shopId){
        List<CouponsDto> result = shopCouponService.enableReceiveCoupons(shopId);
        return ResultVo.ok(result);
    }

    @ApiOperation(value = "用户获取优惠券")
    @GetMapping("/get/{couponId}")
    public ResultVo<Integer> get(@PathVariable Integer couponId){
        Integer userId = MemberContextLocal.get().getId();
        Integer result = shopCouponService.userGetCoupon(couponId,userId);
        return ResultVo.ok(result);
    }

}
