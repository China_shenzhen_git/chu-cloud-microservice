package com.chu.cloud.test;

import com.chu.cloud.repository.ProductPointPlanRepository;
import com.chu.cloud.service.ProductService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.Optional;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/9/12 15:09
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class PointProductTest {

    @Autowired
    private ProductPointPlanRepository productPointPlanRepository;

    @Autowired
    private ProductService productService;

    @Test
    public void test(){
        Optional.ofNullable(productPointPlanRepository.findByProductId(1))
                .filter(pointPlan -> pointPlan.getEndTime().compareTo(new Date()) > 0)
                .ifPresent(pointPlan -> System.out.println(pointPlan));
    }

    @Test
    public void cleaner(){
        productService.cleanerExpirePointProduct();
    }
}
