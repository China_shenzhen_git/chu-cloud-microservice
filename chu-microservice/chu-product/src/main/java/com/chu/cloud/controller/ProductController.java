package com.chu.cloud.controller;

import com.chu.cloud.dto.ProductPointPlanDto;
import com.chu.cloud.dto.order.OrderItemDto;
import com.chu.cloud.entity.Product;
import com.chu.cloud.entity.ProductPointPlan;
import com.chu.cloud.repository.ProductPointPlanRepository;
import com.chu.cloud.repository.ProductRepository;
import com.chu.cloud.service.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/5/16 17:03
 * @Description:
 */
@RestController
@RequestMapping("/v1/product")
@Api(value = "商品相关API")
public class ProductController{

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductPointPlanRepository productPointPlanRepository;

    @PostMapping("/reserve")
    @ApiOperation(value = "预留订单商品")
    public List<ProductPointPlanDto> reserve(@RequestBody List<OrderItemDto> list){
        return productService.reserveProducts(list);
    }

    @ApiOperation(value = "活动商品锁定库存",httpMethod = "POST")
    @PostMapping("/lock/inventory/{productId}/{quantity}")
    public Boolean lockInventory(@PathVariable Integer productId,@PathVariable Integer quantity){
        return productService.lockInventory(productId,quantity);
    }

    @ApiOperation(value = "发布商品")
    @PostMapping("/save")
    public Integer save(@RequestBody Product product){
        return productService.save(product);
    }

    @PostMapping("/ids/in")
    @ApiOperation(value = "查询商品")
    public List<Product> findByIdIn(@RequestBody List<Integer> ids){
        return productRepository.findByIdIn(ids.toArray(new Integer[ids.size()]));
    }

    @ApiOperation(value = "上架积分商品")
    @PostMapping("up/point")
    public Integer pointProduct(@RequestBody ProductPointPlan pointPlan){
        return productPointPlanRepository.save(pointPlan).getId();
    }
}
