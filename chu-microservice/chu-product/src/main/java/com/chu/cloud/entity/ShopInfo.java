package com.chu.cloud.entity;

import com.chu.cloud.base.AbstractEntity;
import com.chu.cloud.enums.ShopGradeEnum;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.io.Serializable;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/5/2 19:30
 * @Description:
 */
@Entity
@Data
@NoArgsConstructor
public class ShopInfo extends AbstractEntity implements Serializable{

    @Column(name = "shop_name")
    private String shopName;

    @Column(name = "shop_sales")
    private Integer shopSales;

    @Enumerated(EnumType.STRING)
    @Column(name = "grade")
    private ShopGradeEnum grade;

    public ShopInfo(String shopName){
        this.shopName = shopName;
        this.shopSales = 0;
        this.grade = ShopGradeEnum.GENERAL;

    }
}
