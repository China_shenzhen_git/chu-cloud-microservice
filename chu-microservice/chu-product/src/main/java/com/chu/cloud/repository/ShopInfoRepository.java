package com.chu.cloud.repository;

import com.chu.cloud.entity.ShopInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/5/3 11:40
 * @Description:
 */
@RepositoryRestResource(path = "shop")
public interface ShopInfoRepository extends JpaRepository<ShopInfo,Integer>{
}
