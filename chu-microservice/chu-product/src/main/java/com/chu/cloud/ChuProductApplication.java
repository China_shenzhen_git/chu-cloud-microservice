package com.chu.cloud;

import com.chu.cloud.stream.channel.OrderProductReturnProcessor;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

@SpringCloudApplication
@EnableBinding({OrderProductReturnProcessor.class})
@EnableScheduling
@EnableHystrix
@EnableJpaAuditing
public class ChuProductApplication {



	public static void main(String[] args) {

		SpringApplication.run(ChuProductApplication.class, args);
	}

	@Bean
	@LoadBalanced
	public RestTemplate restTemplate(){
		return new RestTemplate();
	}
}
