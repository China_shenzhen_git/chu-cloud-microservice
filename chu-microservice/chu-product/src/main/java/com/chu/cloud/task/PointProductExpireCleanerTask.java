package com.chu.cloud.task;

import com.chu.cloud.redis.RedisLockUtil;
import com.chu.cloud.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 *  积分商品过期下架处理任务
 * @author: Tianshu.CHU
 * @Date: 2018/9/12 14:59
 */
@Component
@Slf4j
public class PointProductExpireCleanerTask {

    private static final String key = "point_product_cleaner_event";

    private static final int TIMEOUT = 3 * 1000;

    private final ProductService productService;

    private final RedisLockUtil redisLockUtil;

    public PointProductExpireCleanerTask(ProductService productService, RedisLockUtil redisLockUtil) {
        this.productService = productService;
        this.redisLockUtil = redisLockUtil;
    }

    @Scheduled(cron = "0 15 15 * * ?")
    public void cleaner(){
        long time = System.currentTimeMillis() + TIMEOUT;
        if (!redisLockUtil.lock(key,String.valueOf(time))){
            return;
        }
        log.info("当前执行下架过期积分商品时间:{}", DateTime.now().toString("yyyy-MM-dd HH:mm:ss"));
        productService.cleanerExpirePointProduct();
        redisLockUtil.unlock(key,String.valueOf(time));
    }
}
