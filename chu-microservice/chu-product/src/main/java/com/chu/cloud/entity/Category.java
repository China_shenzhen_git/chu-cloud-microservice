package com.chu.cloud.entity;

import com.chu.cloud.base.AbstractEntity;
import lombok.Data;

import javax.persistence.Entity;
import java.io.Serializable;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/5/4 17:52
 * @Description:
 */
@Entity
@Data
public class Category extends AbstractEntity implements Serializable{


    private String categoryName;

    private Integer parentId;

}
