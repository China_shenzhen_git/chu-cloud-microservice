package com.chu.cloud.entity;

import com.chu.cloud.base.AbstractEntity;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/5/16 15:47
 * @Description: 商品积分计划
 */
@Entity
@Data
@Table(name = "product_point_plan",indexes = {@Index(name = "product_id_unique",columnList = "product_id",unique = true)})
public class ProductPointPlan extends AbstractEntity implements Serializable{

    @Column(name = "product_id")
    private Integer productId;

    @Column(name = "point")
    private Integer point;

    @Column(name = "start_time")
    private Date startTime;

    @Column(name = "end_time")
    private Date endTime;

    @Version
    @Column(name = "version")
    private Long version;

}
