package com.chu.cloud.entity;

import com.chu.cloud.base.AbstractEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Version;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description:
 * @author: TianShu.CHU
 * @CreateDate: 2018-05-06 17:24
 * @Version: 1.0
 */
@Entity
@Data
public class Product extends AbstractEntity implements Serializable{


    private String productName;

    private Integer shopId;

    @Column(name = "category_id")
    private Integer categoryId;

    @Column(name = "price")
    private BigDecimal price;

    @Column(name = "stocks")
    private Integer stocks;

    @Column(name = "comment")
    private String comment;

    /** 0正常,1下架**/
    private Integer status;

    @Version
    @Column(name = "version")
    private Long version;

}
