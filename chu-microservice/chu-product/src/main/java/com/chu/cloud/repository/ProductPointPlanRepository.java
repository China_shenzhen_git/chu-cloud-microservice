package com.chu.cloud.repository;

import com.chu.cloud.entity.ProductPointPlan;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/5/21 17:16
 * @Description:
 */
public interface ProductPointPlanRepository extends JpaRepository<ProductPointPlan,Integer>{

    ProductPointPlan findByProductId(Integer productId);

    List<ProductPointPlan> findByEndTimeLessThan(Date curDate);
}
