package com.chu.cloud.repository;

import com.chu.cloud.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product,Integer>{

//    Product[] findByIdIn(@Param("productIds") Integer... productIds);


    List<Product> findByIdIn(Integer[] ids);

}
