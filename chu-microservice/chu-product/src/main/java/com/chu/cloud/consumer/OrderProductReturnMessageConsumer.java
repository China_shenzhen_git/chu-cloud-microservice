package com.chu.cloud.consumer;

import com.chu.cloud.service.ProductService;
import com.chu.cloud.stream.channel.OrderProductReturnProcessor;
import com.chu.cloud.stream.consumer.MessageConsumer;
import com.chu.cloud.stream.payload.OrderProductReturnDto;
import com.chu.cloud.util.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/6/1 14:31
 * @Description:
 */
@Slf4j
@Component
public class OrderProductReturnMessageConsumer implements MessageConsumer<String>{

    @Autowired
    private ProductService productService;


    @StreamListener(OrderProductReturnProcessor.INPUT)
    @Override
    public void recevieMessage(String payLoad) {
        log.info("商品微服务收到消息:{}",payLoad);
        try {
            List<OrderProductReturnDto> list = JsonUtils.json2list(payLoad, OrderProductReturnDto.class);
            productService.returnProduct(list);
        }catch (Exception e){
            log.error("出异常了:",e);
        }

    }
}
