package com.chu.cloud.controller;

import com.chu.cloud.entity.ShopInfo;
import com.chu.cloud.repository.ShopInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/5/3 11:38
 * @Description:
 */
@RestController
@RequestMapping("/v1/shop")
public class ShopController {

    @Autowired
    private ShopInfoRepository shopInfoRepository;

    @PostMapping("/create/{shopName}")
    public ShopInfo create(@PathVariable String shopName){
        ShopInfo shopInfo = new ShopInfo(shopName);
        shopInfo = shopInfoRepository.save(shopInfo);
        return shopInfo;
    }

}
