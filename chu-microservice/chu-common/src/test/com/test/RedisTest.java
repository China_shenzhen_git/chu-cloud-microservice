package com.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.TimeUnit;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/7/19 15:53
 * @Description:
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisTest {

    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    public void test(){
        String key = "test:times";
        Long increment = redisTemplate.opsForValue().increment(key, 1L);
        if (increment == 1){
            redisTemplate.expire(key,1, TimeUnit.MINUTES);
        }
        System.out.println(increment);
        Object o = redisTemplate.opsForValue().get(key);
        System.out.println(o);
    }


}
