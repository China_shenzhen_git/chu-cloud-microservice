package com.chu.cloud.enums;

import java.io.Serializable;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/4/25 10:55
 * @Description:
 */
public enum ResultEnums implements Serializable {
    USER_NOT_VALID(1001, "用户未认证"),
    ALIDATE_DUCE(1002, "验证码过期"),
    USER_DISABLED(1003, "用户已被禁用"),
    /**
     * 用于更改errorMsg
     **/
    SERVER_ERROR(500, "服务出错"), SUCCESS(200, "成功"),


    OPEN_SERVER_FAIL(502,"聚合微服务API不可用")

    ;

    //


    private Integer responseCode;

    private String responseMsg;

    ResultEnums(Integer responseCode, String responseMsg) {
        this.responseCode = responseCode;
        this.responseMsg = responseMsg;
    }

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public static ResultEnums valueOf(int statusCode) {
        ResultEnums[] var1 = values();

        for (int i = 0; i < var1.length; i++) {
            ResultEnums resultEnums = var1[i];
            if (resultEnums.responseCode == statusCode){
                return resultEnums;
            }
        }

        throw new IllegalArgumentException("No matching constant for [" + statusCode + "]");
    }

}
