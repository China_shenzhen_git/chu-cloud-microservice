package com.chu.cloud.rabbit.dead;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/8/16 18:03
 * @Description:
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class DelayMessagePayload implements Serializable{

//    private String forWardExchange;

    private String forWardQueue;

    private String deadExchange;

    private String deadQueue;

    private String message;

    private long delayTime;

}
