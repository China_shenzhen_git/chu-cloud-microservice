package com.chu.cloud.feign.config;

import feign.Feign;
import feign.InvocationHandlerFactory;
import feign.Request;
import feign.Retryer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/8/24 11:47
 * @Description:
 */
@Component
public class OrderFeignConfig extends BasicFeignConfiguration{



    /**
     * 订单微服务关闭重试机制,避免重复创建订单
     * @return
     */
    @Bean
    public Retryer feignRetryer(){
        return Retryer.NEVER_RETRY;
    }

}
