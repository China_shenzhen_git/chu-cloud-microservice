package com.chu.cloud.util;

import java.util.Random;
import java.util.UUID;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/3/28 10:36
 * @Description: 字符串工具类
 */
public class StrUtil {


    /**
     * 生成token
     *
     * @return
     */
    public String generateToken() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    /**
     * 获取指定位数
     *
     * @param charCount 位数
     * @return
     */
    public String getRandNum(int charCount) {
        StringBuilder charValue = new StringBuilder();
        for (int i = 0; i < charCount; i++) {
            int r = randomInt(0, 10);
            charValue.append(r);
        }
        return charValue.toString();
    }

    /**
     * 范围内随机数
     *
     * @param from 开始
     * @param to   结束
     * @return
     */
    public int randomInt(int from, int to) {
        Random r = new Random();

        return from + r.nextInt(to - from);
    }

}

