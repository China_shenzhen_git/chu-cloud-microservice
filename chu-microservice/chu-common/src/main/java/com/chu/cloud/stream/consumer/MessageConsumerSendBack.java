package com.chu.cloud.stream.consumer;

/**
 * @Description: 消费完之后反馈给对方一个消息的接口
 * @author: TianShu.CHU
 * @CreateDate: 2018-04-15 17:20
 * @Version: 1.0
 */
public interface MessageConsumerSendBack<T> {

    /**
     *  接收到消息并反馈给对方一个消息
     * @param payload
     * @return
     */
    Object recevieWithSendBackMessage(T payload);
}
