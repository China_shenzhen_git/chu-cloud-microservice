package com.chu.cloud.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JsonUtils {
    private final static ObjectMapper objectMapper = new ObjectMapper();
	static {
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false); // 忽略不存在的属性
		objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL); // 转成json字符串时只包含有值的属性
		SimpleDateFormat dateFormat = new SimpleDateFormat(DateUtils.DATETIME_PATTERN);
		objectMapper.setDateFormat(dateFormat); // 设置日期格式 
	}
	
    private JsonUtils() {  
    }  
  
    public static ObjectMapper getInstance() {  
  
        return objectMapper;  
    }  
  
    /** 
     * javaBean,list,array convert to json string 
     */  
    public static String obj2json(Object obj) {
        try {
			return objectMapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
    }  
  
    /** 
     * json string convert to javaBean 
     */  
    public static <T> T json2pojo(String jsonStr, Class<T> clazz)  
            throws Exception {  
        return objectMapper.readValue(jsonStr, clazz);  
    }  
  
    /** 
     * json string convert to map 
     */  
    @SuppressWarnings("unchecked")
	public static <T> Map<String, Object> json2map(String jsonStr)
            throws Exception {  
        return objectMapper.readValue(jsonStr, Map.class);  
    }  
  
    /** 
     * json string convert to map with javaBean 
     */  
    public static <T> Map<String, T> json2map(String jsonStr, Class<T> clazz)  
            throws Exception {  
        Map<String, Map<String, Object>> map = objectMapper.readValue(jsonStr,  
                new TypeReference<Map<String, T>>() {
                });  
        Map<String, T> result = new HashMap<String, T>();
        for (Map.Entry<String, Map<String, Object>> entry : map.entrySet()) {
            result.put(entry.getKey(), map2pojo(entry.getValue(), clazz));  
        }  
        return result;  
    }  
  
    /** 
     * json array string convert to list with javaBean 
     */  
    public static <T> List<T> json2list(String jsonArrayStr, Class<T> clazz)
            throws Exception {  
        List<Map<String, Object>> list = objectMapper.readValue(jsonArrayStr,  
                new TypeReference<List<T>>() {  
                });  
        List<T> result = new ArrayList<T>();
        for (Map<String, Object> map : list) {  
            result.add(map2pojo(map, clazz));  
        }  
        return result;  
    }  
  
    /** 
     * map convert to javaBean 
     */  
    @SuppressWarnings("rawtypes")
	public static <T> T map2pojo(Map map, Class<T> clazz) {  
        return objectMapper.convertValue(map, clazz);  
    }

}
