package com.chu.cloud.stream.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @Description: 订单使用优惠券,改变优惠券状态
 * @author: TianShu.CHU
 * @CreateDate: 2018-05-04 0:07
 * @Version: 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderCouponUseLoad implements Serializable{

    private String orderNo;

    private List<Integer> couponIds;

}
