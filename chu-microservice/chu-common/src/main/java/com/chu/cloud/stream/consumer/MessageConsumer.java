package com.chu.cloud.stream.consumer;


/**
 * 消费订阅者的接口定义
 */
public interface MessageConsumer<T> {

    /**
     * 接收到消息被调用的方法
     * @param payLoad
     */
    void recevieMessage(T payLoad);


}
