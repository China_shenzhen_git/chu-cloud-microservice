package com.chu.cloud.vo;

import com.chu.cloud.enums.CartEventType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @author: TianShu.CHU
 * @CreateDate: 2018-05-06 16:28
 * @Version: 1.0
 */
@Data
public class ShoppingCartVo implements Serializable{

    @ApiModelProperty(hidden = true)
    private Integer id;

    private Integer productId;

    @ApiModelProperty(hidden = true)
    private Integer memberId;

    private Integer shopId;

    private Integer num;

    @ApiModelProperty(hidden = true)
    private CartEventType cartEvent;

}
