package com.chu.cloud.feign.client;

import com.chu.cloud.dto.ProductPointPlanDto;
import com.chu.cloud.dto.order.OrderItemDto;
import com.chu.cloud.feign.MicroServiceName;
import com.chu.cloud.feign.config.BasicFeignConfiguration;
import com.chu.cloud.response.ResponseMessage;
import com.chu.cloud.response.ResultVo;
import com.chu.cloud.vo.OrderItemVo;
import com.chu.cloud.vo.ProductVo;
import feign.Param;
import feign.RequestLine;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.hateoas.Resources;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/5/16 17:06
 * @Description:
 */
@FeignClient(name = MicroServiceName.PRODUCT_SERVICE_INSTANCE,path = "/v1/product",
        configuration = BasicFeignConfiguration.class)
public interface IProductFeignClient {


    @RequestLine("POST /reserve")
    ResponseMessage<List<ProductPointPlanDto>> reserveProduct(@RequestBody List<OrderItemDto> list);

    @RequestLine("POST /product")
    Integer add(@RequestBody ProductVo product);

    @RequestLine("POST /lock/inventory/{productId}/{quantity}")
    ResponseMessage<Boolean> lockInventory(@Param("productId")Integer productId,@Param("quantity")Integer quantity);

    @RequestLine("GET /up/all")
    ResponseMessage<List<ProductVo>> findUpAll();

    @RequestLine("POST /ids/in")
    ResponseMessage<List<ProductVo>> findByIdIn(@RequestBody List<Integer> ids);


}
