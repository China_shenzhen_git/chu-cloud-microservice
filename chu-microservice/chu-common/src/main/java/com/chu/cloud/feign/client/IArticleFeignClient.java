package com.chu.cloud.feign.client;

import com.chu.cloud.feign.MicroServiceName;
import com.chu.cloud.feign.config.BasicFeignConfiguration;
import com.chu.cloud.stream.payload.ArticleLoad;
import feign.Param;
import feign.RequestLine;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/4/25 19:50
 * @Description:
 */
@FeignClient(name = MicroServiceName.MARKETING_SERVICE_INSTANCE,path = "v1/article",
        configuration = BasicFeignConfiguration.class)
public interface IArticleFeignClient {


    @RequestLine("GET /{id}")
    Map<String,Object> findById(@Param("id")String id);

    @RequestLine("GET /list")
    ResponseEntity<List<ArticleLoad>> list();
}
