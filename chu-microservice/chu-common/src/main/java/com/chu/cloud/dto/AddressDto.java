package com.chu.cloud.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/5/9 17:53
 * @Description:
 */
@Data
@NoArgsConstructor
public class AddressDto implements Serializable{

    private Integer id;

    private Integer userId;

    private String consignee;

    private String mobile;

    private Integer provinceId;

    private Integer cityId;

    private Integer regionId;

    private String address;

    private boolean defaultAddress;

    private boolean isDelete;

    public AddressDto(String consignee, String mobile, String address) {
        this.consignee = consignee;
        this.mobile = mobile;
        this.address = address;
    }
}
