package com.chu.cloud.stream.channel;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/9/4 11:26
 * @Description: 订单支付成功广播通道
 */
public interface OrderPaySuccessProcessor {


    String OUTPUT = "order_pay_success_output";

    String USER_POINT = "user_point_incr";

    String SHOP_SALE = "shop_sale_incr";

    String SHIPMENT = "shipment_input";

    @Input(USER_POINT)
    SubscribableChannel userPoint();

    @Input(SHOP_SALE)
    SubscribableChannel shopSale();

    @Input(SHIPMENT)
    SubscribableChannel shipment();

    @Output(OUTPUT)
    MessageChannel output();

}
