//package com.chu.cloud.util;
//
//import com.chu.cloud.vo.ProductVo;
//import com.chu.cloud.vo.ShopInfo;
//import com.chu.cloud.vo.UserVo;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.cloud.client.loadbalancer.LoadBalanced;
//import org.springframework.context.annotation.Bean;
//import org.springframework.core.ParameterizedTypeReference;
//import org.springframework.hateoas.PagedResources;
//import org.springframework.http.HttpMethod;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Component;
//import org.springframework.web.client.RestTemplate;
//
//import java.util.Collection;
//
//
///**
// * @author: Tianshu.CHU
// * @Date: 2018/5/9 19:32
// * @Description: 自动装配类
// */
//@Component
//@Slf4j
//public class RestTemplateUtil {
//
//    @Autowired
//    private RestTemplate restTemplate;
//
//
//    public <T>ResponseEntity<T> getForEntity(String remoteUrl, Class<T> clazz) {
//        return restTemplate.getForEntity(remoteUrl, clazz);
//
//    }
//
//}
