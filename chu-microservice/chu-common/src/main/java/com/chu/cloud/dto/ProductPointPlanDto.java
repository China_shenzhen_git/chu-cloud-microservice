package com.chu.cloud.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/5/16 16:39
 * @Description:
 */
@Data
public class ProductPointPlanDto implements Serializable{


    private Integer productId;

    private Integer quantity;

    private BigDecimal price;

    private Integer rewardPoint;

}
