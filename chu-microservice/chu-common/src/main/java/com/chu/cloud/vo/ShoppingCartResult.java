package com.chu.cloud.vo;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.Data;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @Description: 用户购物车列表
 * @author: TianShu.CHU
 * @CreateDate: 2018-05-06 22:42
 * @Version: 1.0
 */
@Data
public class ShoppingCartResult implements Serializable{

    private List<ShopCart> shopCart ;

    /**失效商品**/
    private List<CartItem> loseProducts;


}
