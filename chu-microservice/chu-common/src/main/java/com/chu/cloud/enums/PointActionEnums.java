package com.chu.cloud.enums;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/8/6 14:59
 * @Description:
 */
public enum PointActionEnums {

    //获得积分
    REWARD,

    //消费积分
    CONSUME,

    ;
}
