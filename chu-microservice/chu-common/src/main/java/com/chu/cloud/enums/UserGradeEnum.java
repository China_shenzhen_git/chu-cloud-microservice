package com.chu.cloud.enums;


/**
 * @author: Tianshu.CHU
 * @Date: 2018/9/4 15:29
 * @Description:
 */
public enum UserGradeEnum {


    V1 {
        @Override
        public UserGradeEnum nextGrade(UserGradeEnum gradeEvent,Integer point) {
            if (gradeEvent == UserGradeEnum.V1 && point.compareTo(100)>=0){
                return UserGradeEnum.V2;
            }
            return UserGradeEnum.V1;
        }
    },

    V2 {
        @Override
        public UserGradeEnum nextGrade(UserGradeEnum gradeEvent,Integer point) {
            if (gradeEvent == UserGradeEnum.V2 && point.compareTo(200)>=0){
                return UserGradeEnum.V3;
            }
            return UserGradeEnum.V1;
        }
    },

    V3 {
        @Override
        public UserGradeEnum nextGrade(UserGradeEnum gradeEvent,Integer point ) {
            if (gradeEvent == UserGradeEnum.V3 && point.compareTo(300)>=0){
                return UserGradeEnum.V3;
            }
            return UserGradeEnum.V2;
        }
    };

    public abstract UserGradeEnum nextGrade(UserGradeEnum gradeEvent,Integer point);


}
