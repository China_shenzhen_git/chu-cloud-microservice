package com.chu.cloud.stream.payload;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/4/13 10:10
 * @Description: 文章载体
 */
@Data
public class ArticleLoad {

    private String id;

    @NotEmpty(message = "标题不能为空")
    private String title;

    @NotEmpty(message = "文章内容不能为空")
    private String article;

    private Integer author;

    @NotEmpty(message = "图片不能为空")
    private String images;

    /**是否置顶,0否,1置顶**/
    private Integer isTop;

    /**是否发布,0否,1发布**/
    private Integer isPub;

    /**商品链接json**/
    private String product;





}
