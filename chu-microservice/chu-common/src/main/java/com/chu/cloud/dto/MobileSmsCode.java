package com.chu.cloud.dto;

import com.chu.cloud.enums.sms.SmsCodeType;
import com.chu.cloud.enums.sms.SmsMessageType;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/3/29 15:45
 * @Description: 发送手机短信DTO
 */
@Data
@AllArgsConstructor
public class MobileSmsCode implements Serializable{

    private String mobile;

    private SmsMessageType template;

    /**使用哪个短信平台发送短信**/
    private SmsCodeType type;

    /**有可能不同的客户端发送的验证码长度不一致,所以由客户端传递**/
    private String code;


    public MobileSmsCode() {
    }
}
