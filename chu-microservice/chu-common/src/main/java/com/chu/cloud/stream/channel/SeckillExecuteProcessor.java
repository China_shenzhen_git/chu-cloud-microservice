package com.chu.cloud.stream.channel;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/9/6 16:10
 * @Description:
 */
public interface SeckillExecuteProcessor {

    String INPUT = "seckill_execute_input";

    String OUTPUT = "seckill_execute_output";

    @Input(INPUT)
    SubscribableChannel input();

    @Output(OUTPUT)
    MessageChannel output();


}
