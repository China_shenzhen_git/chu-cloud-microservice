package com.chu.cloud.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class MemberGetCouponDto implements Serializable {


    private static final long serialVersionUID = 4624889365056684950L;

    private Integer id;

    private Integer shopId;

    private Integer memberId;

    @ApiModelProperty(value = "优惠劵名称")
    private String discountName;

    @ApiModelProperty(value = "张数数量")
    private Integer number;


    @ApiModelProperty(value = "条件（满多少）")
    private BigDecimal conditionMoney;


    @ApiModelProperty(value = "优惠（减多少）")
    private BigDecimal discountMoney;


    @ApiModelProperty(value = "开始时间")
    private Date startTime;


    @ApiModelProperty(value = "结束时间")
    private Date endTime;



    @ApiModelProperty(value = "有效期 开始时间")
    private Date effectStartTime;


    @ApiModelProperty(value = "有效期 结束时间")
    private Date effectEndTime;


    @ApiModelProperty(value = " 每人限领")
    private Integer limitReceive;


    @ApiModelProperty(value = " 代金券图片")
    private String couponPicUrl;


    @ApiModelProperty(value = "状态（1 未开始，2 发行中，3 过期）", required = true)
    private Integer status;


    @ApiModelProperty(value = "版本", required = true)
    private Integer version;


    @ApiModelProperty(value = "描述", required = true)
    private String remark;



    @ApiModelProperty(value = "创建时间", required = true)
    private Date createTime;


    @ApiModelProperty(value = "修改时间", required = true)
    private Date updateTime;

}