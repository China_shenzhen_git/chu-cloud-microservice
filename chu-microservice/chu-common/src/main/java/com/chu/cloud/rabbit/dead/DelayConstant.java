package com.chu.cloud.rabbit.dead;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/8/17 09:49
 * @Description: Rabbit常量类
 */
public class DelayConstant {



    /**
     * 发布订阅队列
     */
    public static class FanoutQueue{
        public static final String SECKILL_DEAD_LETTER = "seckill.dead.letter";
        public static final String SECKILL_TIMEOUT_PAY = "seckill.timeout.pay";
        public static final String ORDER_DELAY_PAY = "order.delay.pay";
        public static final String ORDER_DELAY_PAY_DEAD = "order.delay.pay.dead";
    }

    /**
     * 发布订阅交换机
     */
    public static class FanoutExchange{
        public static final String SECKILL_DEAD_LETTER = "seckill.dead.letter";
        public static final String SECKILL_TIMEOUT_PAY_DEAD = "seckill.timeout.pay.dead";
        public static final String ORDER_DELAY_PAY = "order.delay.pay";
        public static final String ORDER_DELAY_PAY_DEAD = "order.delay.pay.dead";
    }

}
