package com.chu.cloud.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/8/14 16:42
 * @Description:
 */
@Data
public class SeckillDto implements Serializable{

    private Integer id;

    private Integer actId;

    private Integer productId;

    private Integer shopId;

    private Integer seckillInventory;

    private Integer skuLimitBuy;

    private Date startTime;

    private Date endTime;

}
