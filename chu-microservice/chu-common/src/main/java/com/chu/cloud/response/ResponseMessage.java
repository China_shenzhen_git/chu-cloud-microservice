package com.chu.cloud.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/6/29 14:25
 * @Description: 微服务feign返回对象
 */
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResponseMessage<T> implements Serializable {

    private int code;

    private String message;

    private T data;

    public ResponseMessage(int code) {
        this.code = code;
    }


    public ResponseMessage(int code, String message) {
        this.code = code;
        this.message = message;
    }


}
