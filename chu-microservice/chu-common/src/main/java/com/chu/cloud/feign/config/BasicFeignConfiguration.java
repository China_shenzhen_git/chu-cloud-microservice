package com.chu.cloud.feign.config;

import feign.Contract;
import feign.Logger;
import feign.codec.Encoder;
import feign.codec.ErrorDecoder;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ConcurrentHashMap;


/**
 * @Description: 基本的feign配置类
 * @author: TianShu.CHU
 * @CreateDate: 2018-04-07 11:09
 * @Version: 1.0
 */
@Configuration
public class BasicFeignConfiguration {

    @Autowired
    private ObjectFactory<HttpMessageConverters> messageConverters;

    private final ConcurrentHashMap<String,String> map = new ConcurrentHashMap<>();

    /**
     * 将契约改为feign原生的默认契约,这样就可以使用feign自带的注解了
     *
     * @return
     */
    @Bean
    public Contract feignContract() {
        return new Contract.Default();
    }

    /**
     * 访问微服务需要密码
     *
     * @return
     */
    @Bean
    public FeignBasicAuthRequestInterceptor requestInterceptor() {
        return new FeignBasicAuthRequestInterceptor();
    }


    /**
     * feign的默认日志级别是NONE,即不输入任何日志信息
     *
     * @return
     */
    @Bean
    public Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }

    /**
     * 解决feign调用编码错误的问题;feign.codec.EncodeException: class xxx is not a type supported by this encoder.
     */
    @Bean
    public Encoder feignEncoder() {
        return new SpringEncoder(this.messageConverters);
    }

    /**
     * 处理feign响应状态码
     **/
    @Bean
    public ErrorDecoder errorDecoder() {
        return new FeignErrorDecoder();
    }


}
