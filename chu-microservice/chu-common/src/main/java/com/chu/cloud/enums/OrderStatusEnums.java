package com.chu.cloud.enums;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/5/11 15:08
 * @Description: 订单枚举
 */
public enum OrderStatusEnums {

    //新建
    CREATE,

    //确认
    CONFIRM,

    //待发货
    WAITING_SHIP,

    //已发货
    SHIPPED,

    //关闭
    CLOSE;
}
