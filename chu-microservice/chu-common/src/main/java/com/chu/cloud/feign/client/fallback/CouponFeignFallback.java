package com.chu.cloud.feign.client.fallback;

import com.chu.cloud.feign.Hystirx.FeignHystrixResponse;
import com.chu.cloud.feign.client.ICouponFeignClient;
import com.chu.cloud.response.ResponseMessage;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/9/12 14:16
 */
@Component
public class CouponFeignFallback implements FallbackFactory<ICouponFeignClient> {

    @Override
    public ICouponFeignClient create(Throwable throwable) {
        return new ICouponFeignClient() {
            @Override
            public ResponseMessage<Integer> userGetCoupon(Integer userId, Integer couponId) {
                return FeignHystrixResponse.hystrixResponse();
            }
        };
    }
}
