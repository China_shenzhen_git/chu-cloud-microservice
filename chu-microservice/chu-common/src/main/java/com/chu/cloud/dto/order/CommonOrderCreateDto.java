package com.chu.cloud.dto.order;

import com.chu.cloud.dto.OrderDetailDto;
import com.chu.cloud.vo.MemberCouponVo;
import com.chu.cloud.vo.ProductVo;
import lombok.Data;

import java.util.List;

/**
 * @Description:
 * @author: TianShu.CHU
 * @CreateDate: 2018-08-09 21:32
 * @Version: 1.0
 */
@Data
public class CommonOrderCreateDto extends BaseOrderCreateDto {


    /**商品列表**/
    private List<OrderItemDto> goodsList;

    /**用户可用平台或店铺优惠券**/
    private List<MemberCouponVo> memberCoupons;


}
