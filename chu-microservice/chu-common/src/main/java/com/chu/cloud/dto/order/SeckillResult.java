package com.chu.cloud.dto.order;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/9/6 17:55
 * @Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SeckillResult extends OrderResult{

    private String orderNo;

    private Integer productId;

    private Integer quantity;
}
