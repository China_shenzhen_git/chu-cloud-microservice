package com.chu.cloud.feign.client.fallback;

import com.chu.cloud.dto.MemberCouponDto;
import com.chu.cloud.feign.Hystirx.FeignHystrixResponse;
import com.chu.cloud.feign.client.IUserFeignClient;
import com.chu.cloud.response.ResponseMessage;
import com.chu.cloud.vo.UserVo;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Description:
 * @author: TianShu.CHU
 * @CreateDate: 2018-04-07 20:34
 * @Version: 1.0
 */
@Component
@Slf4j
public class UserFeignClientFallback implements FallbackFactory<IUserFeignClient>{
    @Override
    public IUserFeignClient create(Throwable cause) {
        return new IUserFeignClient() {
            @Override
            public ResponseMessage<String> login(String mobile, String validateCode, String ip) {
                log.error("触发服务降级{}",cause.getMessage());
                return FeignHystrixResponse.hystrixResponse();
            }

            @Override
            public ResponseMessage<UserVo> findByToken(String token) {
                return FeignHystrixResponse.hystrixResponse();
            }

            @Override
            public ResponseMessage<List<UserVo>> list() {
                return FeignHystrixResponse.<List<UserVo>>hystrixResponse();
            }

            @Override
            public ResponseMessage<List<MemberCouponDto>> findByIdIn(List<Integer> ids) {
                return FeignHystrixResponse.hystrixResponse();
            }
        };
    }
}
