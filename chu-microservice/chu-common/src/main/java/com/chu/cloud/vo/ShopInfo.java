package com.chu.cloud.vo;

import com.chu.cloud.enums.ShopGradeEnum;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description:
 * @author: TianShu.CHU
 * @CreateDate: 2018-05-06 22:51
 * @Version: 1.0
 */
@Data
@NoArgsConstructor
public class ShopInfo implements Serializable{

    private Integer id;
    private String shopName;

    private Integer shopSales;
    private ShopGradeEnum grade;


}
