package com.chu.cloud.dto.order;

import com.chu.cloud.dto.AddressDto;
import com.chu.cloud.dto.OrderDetailDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/8/24 15:19
 * @Description:
 */
@Data
@NoArgsConstructor
public class OrderResult implements Serializable{

    private AddressDto addressDto;

    List<OrderDetailDto> orderDetaiList;

    public OrderResult(List<OrderDetailDto> orderDetaiList) {
        this.orderDetaiList = orderDetaiList;
    }

    public OrderResult(AddressDto addressDto, List<OrderDetailDto> orderDetaiList) {
        this.addressDto = addressDto;
        this.orderDetaiList = orderDetaiList;
    }
}
