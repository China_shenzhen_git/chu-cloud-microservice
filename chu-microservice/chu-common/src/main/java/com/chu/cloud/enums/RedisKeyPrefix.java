package com.chu.cloud.enums;

import lombok.Getter;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/3/27 14:06
 * @Description: redis前缀枚举类
 */
@Getter
public enum RedisKeyPrefix {

    REDIS_TEST("redis:test:","测试redis缓存"),
    TOKEN_MEMBER("token:member:","用户登录成功后将token和member绑定"),
    MOBILE_SMSCODE("mobile:smscode:","手机验证码缓存"),
    ACTIVITY_PRODUCT_TAG("activity:product:tag:","活动商品标签缓存"),
    SMS_MINUTE_LIMIT_LOCK("sms:minute:lock:","短信分钟级控限制Key"),
    SMS_HOUR_LIMIT_LOCK("sms:hour:lock:","短信时钟级控限制Key"),
    SMS_DAY_LIMIT_LOCK("sms:day:lock:","短信时天级控限制Key"),
    SMS_HOUR_REQUEST_TIMES("sms:hour:request:times:","一小时内请求发送短信次数"),
    SMS_DAY_REQUEST_TIMES("sms:day:request:times:","一天内请求发送短信次数"),
    PRODUCT_STOCKS("product:stocks","商品库存缓存key"),
    USER_ID("user:id:", "用户id缓存"), PRODUCT_ID("product:id:","商品库存缓存"),
    //-----------活动商品缓存
    SECKILL_PRICE("seckill:price:", "秒杀商品价格缓存"),
    SECKILL("seckill:", "秒杀商品前缀"),
    SECKILL_STOCKS("seckill:stocks:", "秒杀商品库存前缀"),
    SECKILL_OPEN_FLAG("seckill:open:flag:","秒杀开关标识"),
    SECKILL_RECORD_USER("seckill:record:user:","用户秒杀记录"),
    SECKILL_RESULT_USER("seckill:result:user:","秒杀成功结果缓存")
    ;



    private String prefix;

    private String message;

    public String getPrefix() {
        return prefix;
    }

    public String getMessage() {
        return message;
    }

    RedisKeyPrefix(String prefix, String message) {
        this.prefix = prefix;
        this.message = message;
    }
}
