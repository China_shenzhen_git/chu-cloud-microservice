package com.chu.cloud.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description: 购物车条目
 * @author: TianShu.CHU
 * @CreateDate: 2018-05-06 22:44
 * @Version: 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CartItem implements Serializable{

    private Boolean effective;

    private Integer cartId;

    private Integer productId;

    private ProductVo product;

    private Integer quantity;

    private BigDecimal productPrice;


}
