package com.chu.cloud.rabbit;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/3/29 15:37
 * @Description: rabbitmq常量
 */
public class RabbitConstants {


    /**
     *  定义点对点直接发送队列名称
     */
    public static class DirectQueueName{

        public static final String MOBILE_CODE_QUEUE = "mobile_code_queue";
        public static final String USER_GET_COUPON = "user_get_coupon";
        public static final String ORDER_COUPON_USE  = "order_coupon_use";
    }

    /**
     * 发布订阅队列名称
     */
    public static class FanoutQueueName{
        public static final String ACTIVITY_TAG_QUEUE = "activity.tag.queue";
        public static final String ORDER_PAY_NOTIFY = "order.pay.notify";
        public static final String SHOPPING_CART_EVENT = "shopping.cart.event";
        public static final String DEAD_EXCHANGE = "dead.exchange";
        public static final String SECKILL_TIMEOUT_PAY = "seckill.timeout.pay";
        public static final String SECKILL_DEAD_LETTER = "seckill.dead.letter";
    }


    @Getter
    public enum DirectQueueNameEnum{

        MOBILE_CODE_QUEUE(DirectQueueName.MOBILE_CODE_QUEUE,"手机短信验证码通道"),

        USER_GET_COUPON(DirectQueueName.USER_GET_COUPON,"用户获取优惠券通道"),

        ORDER_COUPON_USE(DirectQueueName.ORDER_COUPON_USE,"订单使用优惠券通道");

        private String value;

        private String message;

        DirectQueueNameEnum(String value, String message) {
            this.value = value;
            this.message = message;
        }
    }

    public static enum TopicQueueNameEnum{

    }

    @Getter
    @AllArgsConstructor
    public enum FanoutExchangeName{
        SHOPPING_CART_EVENT(FanoutQueueName.SHOPPING_CART_EVENT,"添加商品购物车通道"),
        ORDER_PAY_NOTIFY(FanoutQueueName.ORDER_PAY_NOTIFY,"订单支付通道"),
        DEAD_EXCHANGE(FanoutQueueName.DEAD_EXCHANGE,"延迟队列交换器"),
        SECKILL_TIMEOUT_PAY(FanoutQueueName.SECKILL_TIMEOUT_PAY,"秒杀延迟队列"),
        SECKILL_DEAD_LETTER(FanoutQueueName.SECKILL_DEAD_LETTER, "秒杀死信队列");
        private String value;
        private String message;

        public String getValue() {
            return value;
        }


    }
}
