package com.chu.cloud.enums;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/5/3 10:32
 * @Description: 店铺等级枚举
 */
public enum  ShopGradeEnum {

    //普通
    GENERAL,

    //高级商家
    HIGH_RANK;

}
