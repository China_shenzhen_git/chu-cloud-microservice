package com.chu.cloud.feign.client;

import com.chu.cloud.feign.MicroServiceName;
import com.chu.cloud.feign.client.fallback.ShoppingCartFeignFallback;
import com.chu.cloud.feign.config.BasicFeignConfiguration;
import com.chu.cloud.response.ResponseMessage;
import com.chu.cloud.vo.CheckoutResult;
import com.chu.cloud.vo.ShoppingCartResult;
import feign.Param;
import feign.RequestLine;
import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * @Description:
 * @author: TianShu.CHU
 * @CreateDate: 2018-05-06 18:10
 * @Version: 1.0
 */
@FeignClient(name = MicroServiceName.ORDER_SERVICE_INSTANCE,path = "v1/cart",
        fallbackFactory = ShoppingCartFeignFallback.class,configuration = BasicFeignConfiguration.class)
public interface IShoppingCartFeignClient {


    @RequestLine("GET /{memberId}")
    ResponseMessage<ShoppingCartResult> getUserCart(@Param("memberId") Integer memberId);

    @RequestLine("POST /checkout/{memberId}")
    ResponseMessage<CheckoutResult> checkoutCart(@Param("memberId") Integer memberId);
}
