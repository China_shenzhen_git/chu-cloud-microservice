package com.chu.cloud.rabbit.fanout.queue;

import com.chu.cloud.rabbit.RabbitConstants;
import com.chu.cloud.rabbit.dead.DelayConstant;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.security.PublicKey;
import java.util.HashMap;
import java.util.Map;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/8/27 14:32
 * @Description:
 */
@Configuration
public class OrderPayDelayConfig {


    @Bean
    public Queue initOrderPayDelayDeadQueue(){
        Map<String,Object> arguments = new HashMap<>(2);
        arguments.put("x-dead-letter-exchange", DelayConstant.FanoutExchange.ORDER_DELAY_PAY);
        arguments.put("x-dead-letter-routing-key",DelayConstant.FanoutQueue.ORDER_DELAY_PAY);
        return new Queue(DelayConstant.FanoutQueue.ORDER_DELAY_PAY_DEAD,true,false,false,arguments);
    }
    @Bean
    public FanoutExchange initOrderPayDeadExchange(){
        return new FanoutExchange(DelayConstant.FanoutExchange.ORDER_DELAY_PAY_DEAD);
    }
    @Bean
    public Binding bindOrderPayDelayDead(){
        return BindingBuilder.bind(initOrderPayDelayDeadQueue()).to(initOrderPayDeadExchange());
    }


    @Bean
    public Queue initOrderPayDelayQueue(){
        return new Queue(DelayConstant.FanoutQueue.ORDER_DELAY_PAY);
    }
    @Bean
    public FanoutExchange initOrderPayDelayExchange(){
        return new FanoutExchange(DelayConstant.FanoutExchange.ORDER_DELAY_PAY);
    }
    @Bean
    public Binding bindOrderPayDelay(){
        return BindingBuilder.bind(initOrderPayDelayQueue()).to(initOrderPayDelayExchange());
    }

}
