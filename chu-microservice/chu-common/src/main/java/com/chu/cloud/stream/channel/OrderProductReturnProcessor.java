package com.chu.cloud.stream.channel;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/6/1 14:15
 * @Description: 返回预留的订单商品通道
 */
public interface OrderProductReturnProcessor {


    /** 使用 . 分隔符会接收不到消息,why???**/
    String INPUT = "order_product_return_input";

    String OUTPUT = "order_product_return_output";


    @Output(OrderProductReturnProcessor.OUTPUT)
    MessageChannel output();

    @Input(OrderProductReturnProcessor.INPUT)
    SubscribableChannel input();

}
