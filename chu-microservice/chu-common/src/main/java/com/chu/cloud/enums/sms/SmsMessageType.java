package com.chu.cloud.enums.sms;

/***
 * 短信验证码类型
 * @author Tianshu.CHU
 *
 */
public enum SmsMessageType {

	/**登录**/
	LOGIN,

	/**找回密码**/
	FORGET_PWD,



}
