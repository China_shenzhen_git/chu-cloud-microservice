package com.chu.cloud.feign.client;

import com.chu.cloud.dto.order.CommonOrderCreateDto;
import com.chu.cloud.dto.order.OrderResult;
import com.chu.cloud.feign.MicroServiceName;
import com.chu.cloud.feign.client.fallback.OrderFeignFallback;
import com.chu.cloud.feign.config.OrderFeignConfig;
import com.chu.cloud.response.ResponseMessage;
import com.chu.cloud.vo.CheckoutResult;
import feign.RequestLine;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;


/**
 * @author: Tianshu.CHU
 * @Date: 2018/6/11 11:20
 * @Description:
 */
@FeignClient(name = MicroServiceName.ORDER_SERVICE_INSTANCE,path = "/v1/order",
                configuration = OrderFeignConfig.class,fallbackFactory = OrderFeignFallback.class)
public interface IOrderFeignClient {


    @RequestLine("POST /create/common")
    ResponseMessage<OrderResult> generate(@RequestBody CommonOrderCreateDto orderCreateDto);


}
