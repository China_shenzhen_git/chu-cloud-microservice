package com.chu.cloud.enums;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/6/8 11:47
 * @Description:支付状态
 */
public enum PayStatusEnums {

    WAITING_PAY,

    PAID
    ;


}
