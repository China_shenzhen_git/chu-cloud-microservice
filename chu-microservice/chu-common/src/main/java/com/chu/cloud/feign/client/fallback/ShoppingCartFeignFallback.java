package com.chu.cloud.feign.client.fallback;

import com.chu.cloud.feign.Hystirx.FeignHystrixResponse;
import com.chu.cloud.feign.client.IShoppingCartFeignClient;
import com.chu.cloud.response.ResponseMessage;
import com.chu.cloud.vo.CheckoutResult;
import com.chu.cloud.vo.ShoppingCartResult;
import com.chu.cloud.vo.ShoppingCartVo;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/6/14 14:13
 * @Description:
 */
@Component
@Slf4j
public class ShoppingCartFeignFallback implements FallbackFactory<IShoppingCartFeignClient>{
    @Override
    public IShoppingCartFeignClient create(Throwable cause) {
        return new IShoppingCartFeignClient() {
            @Override
            public ResponseMessage<ShoppingCartResult> getUserCart(Integer memberId) {
                log.error("触发服务降级原因",cause);
                return FeignHystrixResponse.hystrixResponse();
            }

            @Override
            public ResponseMessage<CheckoutResult> checkoutCart(Integer memberId) {
                log.error("触发服务降级原因",cause);
                return FeignHystrixResponse.hystrixResponse();
            }
        };
    }
}
