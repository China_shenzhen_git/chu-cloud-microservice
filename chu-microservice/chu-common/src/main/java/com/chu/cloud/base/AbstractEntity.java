package com.chu.cloud.base;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.Date;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/9/3 14:46
 * @Description: 抽象的实体父类
 */
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@Data
public abstract class AbstractEntity {

    @Id
    @GeneratedValue
    public Integer id;

    @CreatedDate
    public Date createdDate;
    @LastModifiedDate
    public Date modifiedDate;

}
