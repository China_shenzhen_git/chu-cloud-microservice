package com.chu.cloud.feign.config;


import com.netflix.hystrix.exception.HystrixBadRequestException;
import feign.Response;
import feign.Util;
import feign.codec.ErrorDecoder;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

/**
 * 解决feign调用异常信息的获取
 */
@Slf4j
public class FeignErrorDecoder implements ErrorDecoder {
    /**
     * <p>
     * 经测试,1.微服务@Valid异常是400的MethodArgumentNotValidException,无法通过ObjectMapper反序列化(无空参构造)
     * 如果通过字符串截取转json等一系列操作,并不优雅
     * 解决思路:
     * 既然是400异常,那么一定是客户端的问题,作为服务方可以不详细告知客户端具体哪个参数传递有问题,跟客户端约定
     * 抛出的HystrixBadRequestException意味着参数不正确,请认真阅读接口文档即可!
     * <p>
     * 2.抛出的自定义异常是500,且Util.toString(response.body().asReader())没有封装抛出的code
     * 3.Assert断言异常是500,IllegalArgumentException,
     * 解决思路:
     * 针对500异常,不走ErrorDecode逻辑,交给全局异常处理!!(弃用)
     *
     *  补充:
     *      微服务进行全局异常处理,可是这个errorDecoder机制有时不会触发,真的很坑,我的初衷是希望在这里处理自定义异常封装进HystrixBadRequestException
     *      ,这样才不会进行服务降级,还是放弃feign的hystrix吧
     *
     * </p>
     *
     * @param methodKey
     * @param response
     * @return
     */
    @Override
    public Exception decode(String methodKey, Response response) {
        try {
            String s = Util.toString(response.body().asReader());
            String reason = response.reason();
            System.out.println(reason);
            System.out.println(s);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // 这里只封装4开头的请求异常
        if (400 <= response.status() && response.status() < 500) {
            return new HystrixBadRequestException("参数不正确");
        }

        return feign.FeignException.errorStatus(methodKey, response);
    }


}