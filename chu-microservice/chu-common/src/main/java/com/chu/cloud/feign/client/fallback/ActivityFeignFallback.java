package com.chu.cloud.feign.client.fallback;

import com.chu.cloud.dto.ActivityDto;
import com.chu.cloud.feign.Hystirx.FeignHystrixResponse;
import com.chu.cloud.feign.client.IActivityFeignClient;
import com.chu.cloud.response.ResponseMessage;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

/**
 * @Description:
 * @author: TianShu.CHU
 * @CreateDate: 2018-04-20 19:05
 * @Version: 1.0
 */
@Component
@Slf4j
public class ActivityFeignFallback implements FallbackFactory<IActivityFeignClient>{
    @Override
    public IActivityFeignClient create(Throwable cause) {
        return new IActivityFeignClient() {
            @Override
            public ResponseMessage<Boolean> reduceInventory(Integer actId, Integer productId, Integer quantity) {
                return FeignHystrixResponse.hystrixResponse();
            }

        };
    }
}
