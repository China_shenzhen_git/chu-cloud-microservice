package com.chu.cloud.feign.client.fallback;

import com.chu.cloud.dto.order.CommonOrderCreateDto;
import com.chu.cloud.dto.order.OrderResult;
import com.chu.cloud.feign.Hystirx.FeignHystrixResponse;
import com.chu.cloud.feign.client.IOrderFeignClient;
import com.chu.cloud.response.ResponseMessage;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/8/24 10:17
 * @Description:
 */
@Component
@Slf4j
public class OrderFeignFallback implements FallbackFactory<IOrderFeignClient>{
    @Override
    public IOrderFeignClient create(Throwable cause) {
        return new IOrderFeignClient() {
            @Override
            public ResponseMessage<OrderResult> generate(CommonOrderCreateDto orderCreateDto) {
                log.error("触发降级原因:",cause);
                return FeignHystrixResponse.hystrixResponse();
            }
        };
    }
}
