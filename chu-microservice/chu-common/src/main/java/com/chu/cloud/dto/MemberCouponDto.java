package com.chu.cloud.dto;

import com.chu.cloud.enums.CouponEnum;
import com.chu.cloud.util.BeanUtil;
import com.chu.cloud.vo.MemberCouponVo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.http.ResponseEntity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/5/9 18:00
 * @Description:
 */
@Data
public class MemberCouponDto implements Serializable{

    private Integer id;

    @ApiModelProperty(value = "优惠劵名称")
    private String couponDiscountName;


    @ApiModelProperty(value = "订单号")
    private String orderNo;


    private CouponEnum status;

    private Date useStartDate;

    @ApiModelProperty(value = "过期时间")
    private Date expirationDate;


    @ApiModelProperty(value = "优惠卷劵ID")
    private Integer couponDiscountId;

    private Integer couponNum;




}
