package com.chu.cloud.dto.order;

import lombok.Data;

/**
 * @Description:
 * @author: TianShu.CHU
 * @CreateDate: 2018-08-13 21:41
 * @Version: 1.0
 */
@Data
public class PointOrderCreateDto extends BaseOrderCreateDto{

    private Integer productId;

    private Integer shopId;

    private Integer quantity;

}
