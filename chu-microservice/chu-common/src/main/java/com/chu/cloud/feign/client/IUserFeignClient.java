package com.chu.cloud.feign.client;

import com.chu.cloud.dto.MemberCouponDto;
import com.chu.cloud.feign.MicroServiceName;
import com.chu.cloud.feign.client.fallback.UserFeignClientFallback;
import com.chu.cloud.feign.config.BasicFeignConfiguration;
import com.chu.cloud.response.ResponseMessage;
import com.chu.cloud.vo.UserVo;
import feign.Param;
import feign.RequestLine;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(name = MicroServiceName.USER_SERVICE_INSTANCE,path = "/v1/users",
        fallbackFactory = UserFeignClientFallback.class,
        configuration = BasicFeignConfiguration.class)
public interface IUserFeignClient {

    @RequestLine("POST /login/{mobile}/{validateCode}/{ip}")
    ResponseMessage<String> login(@Param("mobile") String mobile, @Param("validateCode") String validateCode, @Param("ip") String ip);


    @RequestLine("GET /find/{token}")
    ResponseMessage<UserVo> findByToken(@Param("token")String token);

    @RequestLine("GET /list")
    ResponseMessage<List<UserVo>> list();

    @RequestLine("POST /coupons")
    ResponseMessage<List<MemberCouponDto>> findByIdIn(@RequestBody List<Integer> ids);

}
