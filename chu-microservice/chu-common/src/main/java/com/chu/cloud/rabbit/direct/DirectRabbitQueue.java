package com.chu.cloud.rabbit.direct;

import com.chu.cloud.rabbit.RabbitConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;



/**
 * @author: Tianshu.CHU
 * @Date: 2018/3/30 14:26
 * @Description: 点对点直接发送配置类
 */
@Configuration
@Slf4j
public class DirectRabbitQueue {

    /**
     * 初始化手机验证码通道
     *
     * @return
     */
    @Bean
    public Queue initMobileCodeQueue() {
        return new Queue(RabbitConstants.DirectQueueName.MOBILE_CODE_QUEUE);
    }

    @Bean
    public Queue initUserGetCouponQueue() {
        return new Queue(RabbitConstants.DirectQueueName.USER_GET_COUPON);
    }

    @Bean
    public Queue initOrderCouponUseQueue() {
        return new Queue(RabbitConstants.DirectQueueName.ORDER_COUPON_USE);
    }


}

