package com.chu.cloud.enums;

import lombok.Getter;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/5/18 10:32
 * @Description:
 */
@Getter
public enum OrderType {



    //普通商品订单
    COMMON("commonOrder","普通订单"),

    //积分商品订单;
    POINT("pointOrder","积分商品订单"),

    //秒杀
    SECKILL("seckillOrder","秒杀订单")

    ;

    private String value;

    private String message;

    OrderType(String value, String message) {
        this.value = value;
        this.message = message;
    }


    public static String defaulGenerateRule(OrderType orderType){
        if (orderType == OrderType.COMMON){
            return "01";
        }
        if (orderType == OrderType.POINT){
            return "02";
        }
        if (orderType == OrderType.SECKILL){
            return "03";
        }
        return "-1";
    }

}
