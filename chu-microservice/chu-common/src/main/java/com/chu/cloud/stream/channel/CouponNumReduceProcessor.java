package com.chu.cloud.stream.channel;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface CouponNumReduceProcessor {

    String INPUT = "coupon_reduce_input";
    String OUTPUT = "coupon_reduce_output";

    @Output(CouponNumReduceProcessor.OUTPUT)
    MessageChannel output();

    @Input(CouponNumReduceProcessor.INPUT)
    SubscribableChannel input();
}
