package com.chu.cloud.response;

import com.chu.cloud.enums.ResultEnums;
import lombok.Data;

import java.io.Serializable;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/4/25 15:20
 * @Description: 响应结构体
 */
@Data
public class ResultVo<T> implements Serializable{

    private Integer code;

    private String message;

    private T data;

    public ResultVo() {
    }

    public ResultVo(T data) {
        this.code = ResultEnums.SUCCESS.getResponseCode();
        this.message = ResultEnums.SUCCESS.getResponseMsg();
        this.data = data;

    }

    public ResultVo(ResultEnums resultEnums) {
        this.code = resultEnums.getResponseCode();
        this.message = resultEnums.getResponseMsg();
    }

    public ResultVo(String errorMessage) {
        this.code = -1;
        this.message = errorMessage;
    }

    public static <T> ResultVo<T> ok(T data){
       return new ResultVo<T>(data);
    }


    public static <T> ResultVo<T> errorOfEnum(ResultEnums resultEnums){
        return new ResultVo<T>(resultEnums);
    }

    /**
     * 调用此方法返回的code都为500
     * @param errorMessage
     * @param <T>
     * @return
     */
    public static <T> ResultVo<T> errorOfMessage(String errorMessage) {
        ResultEnums resultEnums = ResultEnums.SERVER_ERROR;
        resultEnums.setResponseMsg(errorMessage);
        return errorOfEnum(resultEnums);
    }
    public static <T> ResultVo<T> getError(int code,String errorMessage) {
        ResultVo resultVo = new ResultVo();
        resultVo.setCode(code);
        resultVo.setMessage(errorMessage);
        return resultVo;
    }

}
