package com.chu.cloud.rabbit.fanout.queue;

import com.chu.cloud.rabbit.RabbitConstants;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/8/9 11:03
 * @Description:
 */
@Configuration
public class ShoppingCartAddFanoutQueue {

    @Bean
    public Queue initAddCartQueue(){
        return new Queue(RabbitConstants.FanoutQueueName.SHOPPING_CART_EVENT);
    }

    @Bean
    public FanoutExchange exchange(){
        return new FanoutExchange(RabbitConstants.FanoutExchangeName.SHOPPING_CART_EVENT.getValue());
    }

    @Bean
    public Binding binding(){
        return BindingBuilder.bind(initAddCartQueue()).to(exchange());
    }

}
