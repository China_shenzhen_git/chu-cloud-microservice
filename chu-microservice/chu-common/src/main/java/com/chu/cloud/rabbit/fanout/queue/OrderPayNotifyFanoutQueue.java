package com.chu.cloud.rabbit.fanout.queue;

import com.chu.cloud.rabbit.RabbitConstants;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * @author: Tianshu.CHU
 * @Date: 2018/6/8 10:53
 * @Description:
 */
@Configuration
public class OrderPayNotifyFanoutQueue {


    @Bean
    public Queue initOrderPayNotifyQueue(){
        return new Queue(RabbitConstants.FanoutQueueName.ORDER_PAY_NOTIFY);
    }

    @Bean
    public FanoutExchange exchange(){
        return new FanoutExchange(RabbitConstants.FanoutExchangeName.ORDER_PAY_NOTIFY.getValue());
    }

    @Bean
    public Binding binding(){
        return BindingBuilder.bind(initOrderPayNotifyQueue()).to(exchange());
    }
}
