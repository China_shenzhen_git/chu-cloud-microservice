package com.chu.cloud.vo;

import com.chu.cloud.enums.CouponEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Date;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/5/10 19:55
 * @Description:
 */
@Data
public class MemberCouponVo implements Serializable{

    private Integer id;

    @ApiModelProperty(value = "优惠劵名称",hidden = true)
    private String couponDiscountName;

    @ApiModelProperty(value = "订单号",hidden = true)
    private String orderNo;

    @ApiModelProperty(hidden = true)
    private CouponEnum status;

    @ApiModelProperty(hidden = true)
    private Integer shopId;

    @ApiModelProperty(hidden = true)
    private Date useStartDate;

    @ApiModelProperty(value = "过期时间",hidden = true)
    private Date expirationDate;

    @ApiModelProperty(value = "优惠卷劵ID",hidden = true)
    private Integer couponDiscountId;
    @ApiModelProperty(hidden = true)
    private Integer couponNum;

    @ApiModelProperty(value = "条件（满多少）",hidden = true)
    private BigDecimal conditionMoney;


    @ApiModelProperty(value = "优惠（减多少）",hidden = true)
    private BigDecimal discountMoney;


    @ApiModelProperty(value = " 代金券图片",hidden = true)
    private String couponPicUrl;


//    @Override
//    public int compareTo(MemberCouponVo o) {
//        return this.getConditionMoney().compareTo(o.getConditionMoney()) > 0 ? 1 : -1;
//    }
}
