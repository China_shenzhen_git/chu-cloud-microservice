package com.chu.cloud.feign;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/4/20 11:44
 * @Description:
 */
public interface MicroServiceName {

    String USER_SERVICE_INSTANCE = "chu-user";

    String MARKETING_SERVICE_INSTANCE = "chu-marketing";

    String ORDER_SERVICE_INSTANCE  = "chu-order";

    String PRODUCT_SERVICE_INSTANCE = "chu-product";
}
