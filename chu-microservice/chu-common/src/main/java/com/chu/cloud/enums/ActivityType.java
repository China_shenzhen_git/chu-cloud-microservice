package com.chu.cloud.enums;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/9/6 14:00
 * @Description:
 */
public enum ActivityType {

    //秒杀
    SECKILL,

    //抢购
    RUSH_BUYING,

    ;

}
