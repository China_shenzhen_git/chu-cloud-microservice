package com.chu.cloud.feign.client;

import com.chu.cloud.dto.ActivityDto;
import com.chu.cloud.feign.MicroServiceName;
import com.chu.cloud.feign.client.fallback.ActivityFeignFallback;
import com.chu.cloud.feign.config.BasicFeignConfiguration;
import com.chu.cloud.response.ResponseMessage;
import feign.Param;
import feign.RequestLine;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/4/20 11:40
 * @Description:
 */
@FeignClient(name = MicroServiceName.MARKETING_SERVICE_INSTANCE,path = "v1/activity",
        configuration = BasicFeignConfiguration.class,fallbackFactory = ActivityFeignFallback.class)
public interface IActivityFeignClient {


    @RequestLine("POST /reduce/inventory/{actId}/{productId}/{quantity}")
    ResponseMessage<Boolean> reduceInventory(@Param("actId")Integer actId,@Param("productId")Integer productId,@Param("quantity")Integer quantity);

}
