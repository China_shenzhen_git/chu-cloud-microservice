package com.chu.cloud.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/4/26 16:10
 * @Description:
 */
@Data
public class ActivityTypeVo implements Serializable{

    private Integer id;

    private String name;

    @ApiModelProperty(value = "套餐价格")
    private java.math.BigDecimal price;

    @ApiModelProperty(value = "0:平台发起活动 1:店铺发起活动")
    private Integer platform;

    private Integer adminId;

}
