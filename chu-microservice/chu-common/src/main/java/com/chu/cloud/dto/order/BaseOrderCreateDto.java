package com.chu.cloud.dto.order;

import com.chu.cloud.enums.OrderType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @author: TianShu.CHU
 * @CreateDate: 2018-08-09 21:29
 * @Version: 1.0
 */
@Data
public abstract class BaseOrderCreateDto implements Serializable{

    private Integer memberId;

    @ApiModelProperty(hidden = true)
    private String ip;

    private Integer addressId;

    private OrderType orderType;

}
