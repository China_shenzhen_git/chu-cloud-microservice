package com.chu.cloud.enums.sms;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/9/20 15:00
 * @Description:
 */
@Getter
@AllArgsConstructor
public enum SmsCodeType {


    ALIYUN("aliyun","阿里云发送短信"),


    TENGXUN("tengxun","腾讯发送短信"),

    ;


    private String type;

    private String message;

}
