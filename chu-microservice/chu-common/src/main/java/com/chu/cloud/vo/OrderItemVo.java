package com.chu.cloud.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/5/11 10:25
 * @Description: 店铺订单详情
 */
@Data
public class OrderItemVo implements Serializable{

    private Integer productId;

    private Integer quantity;

    private ProductVo product;


}
