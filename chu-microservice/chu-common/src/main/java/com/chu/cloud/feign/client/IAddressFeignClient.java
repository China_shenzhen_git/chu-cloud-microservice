package com.chu.cloud.feign.client;

import com.chu.cloud.dto.AddressDto;
import com.chu.cloud.feign.MicroServiceName;
import com.chu.cloud.feign.client.fallback.AddressFeignClientFallback;
import com.chu.cloud.feign.config.BasicFeignConfiguration;
import com.chu.cloud.response.ResponseMessage;
import feign.Param;
import feign.RequestLine;
import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/8/20 13:46
 * @Description:
 */
@FeignClient(value = MicroServiceName.USER_SERVICE_INSTANCE,path = "v1/address",
        configuration = BasicFeignConfiguration.class,fallbackFactory = AddressFeignClientFallback.class)
public interface IAddressFeignClient {

    @RequestLine("GET /{id}")
    ResponseMessage<AddressDto> findByAddressId(@Param("id") Integer id);

    @RequestLine("GET /default/{userId}")
    ResponseMessage<AddressDto> findByUserIdDefault(@Param("userId")Integer userId);
}
