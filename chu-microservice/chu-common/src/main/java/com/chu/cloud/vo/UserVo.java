package com.chu.cloud.vo;

import com.chu.cloud.enums.UserGradeEnum;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/4/13 10:43
 * @Description:
 */
@Data
public class UserVo implements Serializable{
    private Integer id;

    private String userName;

    private String mobile;

    private UserGradeEnum grade;

    private Integer status;

    private Integer loginNum;

    private Date registerTime;

    private String registerIp;

    public Date createdDate;

    public Date modifiedDate;
}
