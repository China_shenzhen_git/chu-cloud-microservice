package com.chu.cloud.util;

import com.chu.cloud.enums.ResultEnums;

/**
 * @Description: 微服务异常基类
 * @author: TianShu.CHU
 * @CreateDate: 2018-04-20 21:07
 * @Version: 1.0
 */
public class ChuException extends RuntimeException {

    //TODO 全局异常拆分 code msg,各微服务继承此异常,最终调用方可以获得原始code
    public ChuException(ResultEnums resultEnums) {
        super(JsonUtils.obj2json(resultEnums));

    }

    public ChuException(String message) {
        super(message);
    }

    public static void throwException(ResultEnums resultEnums){

        throw new ChuException(resultEnums);
    }

}
