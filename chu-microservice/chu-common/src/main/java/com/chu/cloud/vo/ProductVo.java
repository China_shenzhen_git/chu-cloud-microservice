package com.chu.cloud.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Description:
 * @author: TianShu.CHU
 * @CreateDate: 2018-05-06 18:00
 * @Version: 1.0
 */
@Data
public class ProductVo implements Serializable{

    private Integer id;

    private String productName;

    private Integer shopId;

    private Integer categoryId;

    private BigDecimal price;

    private Integer stocks;

    private Integer status;

    private String comment;

    private Integer point;


}
