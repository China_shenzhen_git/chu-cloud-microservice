package com.chu.cloud.rabbit.fanout.send;

import com.chu.cloud.rabbit.dead.DelayMessagePayload;
import com.chu.cloud.rabbit.RabbitConstants.FanoutExchangeName;
import com.chu.cloud.util.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.support.CorrelationData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.UUID;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/5/30 11:27
 * @Description: 发送广播消息
 */
@Slf4j
@Component
public class RabbitFanoutSender {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 发送广播消息
     *
     * @param exchange 所有绑定在此交换器上的队列都可以接收到广播消息
     * @param message  发送的消息体
     */
    public void sendFanoutMessage(FanoutExchangeName exchange, Object message) {
        Assert.notNull(exchange, "exchange can not be null");
        Assert.notNull(message, "message can not be null");
        CorrelationData correlationData = new CorrelationData(UUID.randomUUID().toString());
        rabbitTemplate.convertAndSend(exchange.getValue(), "", message, correlationData);
        log.info("fanout send success exchange:{}, message:{}, correlationId:{}", exchange.getValue(),
                JsonUtils.obj2json(message), correlationData.getId());

    }

    /**
     * 延迟发送消息队列,客户端发目标队列发送一条延迟消息 : {"queue":"queueName","body":"message"}
     * 此时,将消息发送到DXL死信队列,而非直接发送到queueName队列,并设置延迟时间 times 秒
     * <p>
     * 死信队列没有消费者,它用来存储超时的消息,并转发到另一队列,转发队列等待消息延迟之后接收到消息,
     *  已过了times 秒,处理业务逻辑
     *
     * @param payload 消息体
     */
    public void sendDelayMessage(DelayMessagePayload payload) {
        CorrelationData correlation = new CorrelationData(UUID.randomUUID().toString());
        rabbitTemplate.convertAndSend(payload.getDeadExchange(), payload, message -> {
            message.getMessageProperties().setExpiration(payload.getDelayTime() + "");
            return message;
        }, correlation);
        log.info("fanout send delay message success exchange:{}, message:{}, correlationId:{}",payload.getDeadExchange(),
                JsonUtils.obj2json(payload), correlation.getId());
    }

}
