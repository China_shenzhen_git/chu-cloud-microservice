package com.chu.cloud.dto.order;

import lombok.Data;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/8/14 15:29
 * @Description:
 */
@Data
public class SeckillOrderCreateDto extends BaseOrderCreateDto{

    private Integer actId;

    private Integer productId;

    private Integer shopId;

    private Integer quantity;

}
