package com.chu.cloud.rabbit.fanout.queue;

import com.chu.cloud.rabbit.dead.DelayConstant;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/8/17 10:44
 * @Description:
 */
@Configuration
public class SeckillTimeOutPayConfig {

    /**
     * 初始化死信队列
     */
    @Bean
    public Queue initSeckillDead(){
        Map<String,Object> arguments = new HashMap<>(2);
        arguments.put("x-dead-letter-exchange", DelayConstant.FanoutExchange.SECKILL_DEAD_LETTER);
        arguments.put("x-dead-letter-routing-key", DelayConstant.FanoutQueue.SECKILL_TIMEOUT_PAY);
        return new Queue(DelayConstant.FanoutQueue.SECKILL_DEAD_LETTER,true,false,false,arguments);
    }
    @Bean
    public FanoutExchange initSeckillDeadExchange(){
        return new FanoutExchange(DelayConstant.FanoutExchange.SECKILL_TIMEOUT_PAY_DEAD);
    }
    @Bean
    public Binding bindSeckillDead(){
        return BindingBuilder.bind(initSeckillDead()).to(initSeckillDeadExchange());
    }

    /**
     * 初始化转发队列
     */
    @Bean
    public FanoutExchange initSeckillForwardExchange(){
        return new FanoutExchange(DelayConstant.FanoutExchange.SECKILL_DEAD_LETTER);
    }
    @Bean
    public Queue initSeckillForwardQueue(){
        return new Queue(DelayConstant.FanoutQueue.SECKILL_TIMEOUT_PAY);
    }
    @Bean
    public Binding bindSeckillForwardExchange(){
        return BindingBuilder.bind(initSeckillForwardQueue()).to(initSeckillForwardExchange());
    }
}
