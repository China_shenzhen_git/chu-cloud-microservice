package com.chu.cloud.feign.client;

import com.chu.cloud.feign.MicroServiceName;
import com.chu.cloud.feign.client.fallback.CouponFeignFallback;
import com.chu.cloud.feign.config.BasicFeignConfiguration;
import com.chu.cloud.response.ResponseMessage;
import feign.Param;
import feign.RequestLine;
import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/5/3 20:24
 * @Description:
 */
@FeignClient(name = MicroServiceName.MARKETING_SERVICE_INSTANCE,path = "/v1/coupons",
        configuration = BasicFeignConfiguration.class,fallbackFactory = CouponFeignFallback.class)
public interface ICouponFeignClient {


    @RequestLine("GET get/{userId}/{couponId}")
    ResponseMessage<Integer> userGetCoupon(@Param("userId") Integer userId, @Param("couponId")Integer couponId);

}


