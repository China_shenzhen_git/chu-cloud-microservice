package com.chu.cloud.feign.config;

import com.chu.cloud.config.MicroServiceInvokeAuth;
import com.chu.cloud.util.EncodeUtils;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description:
 * @author: TianShu.CHU
 * @CreateDate: 2018-04-07 23:59
 * @Version: 1.0
 */
public class FeignBasicAuthRequestInterceptor implements RequestInterceptor {

    @Autowired
    private MicroServiceInvokeAuth serviceConfig;

    public FeignBasicAuthRequestInterceptor() {
    }

    @Override
    public void apply(RequestTemplate requestTemplate) {
        String auth = serviceConfig.getUserUsername() + ":" + serviceConfig.getUserPassword();
        requestTemplate.header("Authorization",EncodeUtils.encodeBase64(auth.getBytes()));
    }

}
