package com.chu.cloud.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/5/9 16:03
 * @Description:
 */
@Data
public class CreateOrderVo implements Serializable{

    /**收货地址**/
    private Integer addressId;

    /** key:shopId,value:要提交的订单项**/
    private Map<Integer,List<OrderItemVo>> orderItemMap;

    /** key:店铺Id,value:优惠券id**/
    private Map<Integer,Integer> shopCouponMap;

    /**是否匿名**/
    private boolean isAnonymous;



}
