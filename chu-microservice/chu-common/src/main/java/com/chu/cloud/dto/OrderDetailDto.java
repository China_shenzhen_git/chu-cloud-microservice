package com.chu.cloud.dto;

import com.chu.cloud.dto.order.OrderItemDto;
import com.chu.cloud.vo.OrderItemVo;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/5/18 09:55
 * @Description:
 */
@Data
@NoArgsConstructor
public class OrderDetailDto implements Serializable{

    private String orderNo;

    private Date orderTime;

//    private List<OrderItemDto> itemDtoList;

    /**订单总价**/
    private BigDecimal totalPrice;

    /**支付金额 = 订单总价 - 优惠金额**/
    private BigDecimal payPrice;

    /**优惠金额**/
    private BigDecimal discountPrice;


}
