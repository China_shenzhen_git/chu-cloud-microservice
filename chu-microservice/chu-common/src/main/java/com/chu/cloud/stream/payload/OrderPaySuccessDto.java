package com.chu.cloud.stream.payload;

import com.chu.cloud.base.AbstractEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/8/28 10:17
 * @Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderPaySuccessDto implements Serializable{

    private Integer memberId;

    private Integer rewardPoint;

    private String orderNo;

}
