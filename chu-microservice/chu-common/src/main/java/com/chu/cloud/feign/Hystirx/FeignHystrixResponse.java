package com.chu.cloud.feign.Hystirx;

import com.chu.cloud.response.ResponseMessage;
import org.springframework.http.HttpStatus;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/7/5 10:32
 * @Description:
 */
public class FeignHystrixResponse {


    public static <T> ResponseMessage<T> hystrixResponse() {
        ResponseMessage<T> responseMessage = new ResponseMessage<T>(HttpStatus.GATEWAY_TIMEOUT.value(), "微服务不可用");
        return responseMessage;
    }


}
