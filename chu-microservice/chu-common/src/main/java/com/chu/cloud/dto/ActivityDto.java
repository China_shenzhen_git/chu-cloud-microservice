package com.chu.cloud.dto;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/4/20 11:49
 * @Description:
 */
@Api(value = "发布活动")
@Data
public class ActivityDto implements Serializable{


    /**
     * id
     */
    @ApiModelProperty(value = "自增")
    private Integer id;

    /**
     * 限制商家报名组Id
     */
    @ApiModelProperty(value = "限制商家报名组Id")
    private Integer shopGroupId;

    /**
     * 活动标题
     */
    @ApiModelProperty(value = "活动标题", required = true)
    private String actTitle;

    /**
     * 活动类型type_id
     */
    @ApiModelProperty(value = "活动类型type_id", required = true)
    private Integer actType;

    /**
     * 活动横幅大图片
     */
    @ApiModelProperty(value = "活动横幅大图片", required = true)
    private String actBanner;


    /**
     * 活动描述
     */
    @ApiModelProperty(value = "活动描述", required = true)
    private String actDesc;

    /**
     * 店铺SPU报名数
     */
    @ApiModelProperty(value = "店铺SPU报名数")
    private Integer signNum;

    /**
     * 参加活动锁定库存
     */
    @ApiModelProperty(value = "参加活动锁定库存")
    private Integer skuMaxNum;

    /**
     * 会员限购数
     */
    @ApiModelProperty(value = "会员限购数")
    private Integer skuLimitNum;

    /**
     * 报名开始时间
     */
    @ApiModelProperty(value = "报名开始时间", required = true)
    private Date signStartTime;

    /**
     * 报名结束时间
     */
    @ApiModelProperty(value = "报名结束时间", required = true)
    private Date signEndTime;

    /**
     * 活动开始时间
     */
    @ApiModelProperty(value = "活动开始时间", required = true)
    private Date startTime;

    /**
     * 活动结束时间
     */
    @ApiModelProperty(value = "活动结束时间", required = true)
    private Date endTime;


    /**
     * 活动状态 0:为关闭 1:(默认)为开启
     */
    @ApiModelProperty(value = "活动状态 0:为关闭 1:(默认)为开启", required = true)
    private Integer actState;

    /**
     * 优惠折扣
     */
    @ApiModelProperty(value = "优惠折扣")
    private Integer discount;

    /**
     * 积分兑换比率
     */
    @ApiModelProperty(value = "积分兑换比率")
    private Integer scoreRebate;


    /**
     * 删除状态 0:(默认)正常 1:已删除
     */
    @ApiModelProperty(value = "删除状态 0:(默认)正常 1:已删除", required = true)
    private Integer isDisabled;

    /**
     * 管理员ID
     */
    @ApiModelProperty(value = "管理员ID")
    private Integer adminId;

}
