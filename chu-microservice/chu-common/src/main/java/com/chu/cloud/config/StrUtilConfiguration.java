package com.chu.cloud.config;


import com.chu.cloud.util.StrUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/9/11 16:46
 */
@Configuration
public class StrUtilConfiguration {

    /**
     * 自动装配{@link com.chu.cloud.util.StrUtil}Bean
     * @return
     */
    @Bean
    public StrUtil strUtil(){
        return new StrUtil();
    }

}
