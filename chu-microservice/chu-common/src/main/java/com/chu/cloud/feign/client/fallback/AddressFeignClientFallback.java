package com.chu.cloud.feign.client.fallback;

import com.chu.cloud.dto.AddressDto;
import com.chu.cloud.feign.Hystirx.FeignHystrixResponse;
import com.chu.cloud.feign.client.IAddressFeignClient;
import com.chu.cloud.response.ResponseMessage;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/8/20 13:52
 * @Description:
 */
@Component
@Slf4j
public class AddressFeignClientFallback implements FallbackFactory<IAddressFeignClient>{
    @Override
    public IAddressFeignClient create(Throwable cause) {
        return new IAddressFeignClient() {
            @Override
            public ResponseMessage<AddressDto> findByAddressId(Integer id) {
                log.error("触发回退原因",cause);
                return FeignHystrixResponse.hystrixResponse();
            }

            @Override
            public ResponseMessage<AddressDto> findByUserIdDefault(Integer userId) {
                log.error("触发回退原因",cause);
                return FeignHystrixResponse.hystrixResponse();
            }
        };
    }
}
