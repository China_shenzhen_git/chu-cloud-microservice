package com.chu.cloud.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/5/10 18:52
 * @Description: 营销卷
 */
@Data
public class DiscountCouponDto implements Serializable{
    @ApiModelProperty(value = "条件（满多少）")
    private BigDecimal conditionMoney;


    @ApiModelProperty(value = "优惠（减多少）")
    private BigDecimal discountMoney;


    @ApiModelProperty(value = "店铺ID")
    private Integer shopId;

}
