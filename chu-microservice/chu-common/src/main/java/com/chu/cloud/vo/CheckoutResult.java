package com.chu.cloud.vo;

import com.chu.cloud.dto.AddressDto;
import com.chu.cloud.dto.MemberGetCouponDto;
import lombok.Data;
import org.springframework.util.CollectionUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/5/8 16:45
 * @Description: 检出购物车
 */
@Data
public class CheckoutResult extends ShoppingCartResult implements Serializable{

    private Integer addressId;

    private AddressDto address;

    private BigDecimal totalPrice;

}
