package com.chu.cloud.stream.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/6/1 14:09
 * @Description: 订单未支付,返回预留商品dto
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderProductReturnDto implements Serializable{

    private String orderNo;

    private Integer productId;

    private Integer quantity;
}
