package com.chu.cloud.dto.order;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description:
 * @author: TianShu.CHU
 * @CreateDate: 2018-08-09 21:38
 * @Version: 1.0
 */
@Data
public class OrderItemDto implements Serializable{

    @ApiModelProperty(value = "商品id")
    private Integer productId;

    @ApiModelProperty(value = "店铺Id")
    private Integer shopId;

    @ApiModelProperty(value = "数量")
    private Integer quantity;

    @ApiModelProperty(hidden = true)
    private BigDecimal productPrice;

    @ApiModelProperty(hidden = true)
    public BigDecimal getItemPrice(){
        if (productPrice == null){
            return new BigDecimal(0);
        }
        return new BigDecimal(quantity).multiply(productPrice);
    }

}
