package com.chu.cloud.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Description:
 * @author: TianShu.CHU
 * @CreateDate: 2018-04-08 20:03
 * @Version: 1.0
 */
@Component
@ConfigurationProperties(prefix = "microservice")
@Data
public class MicroServiceInvokeAuth {

    private String userUsername;

    private String userPassword;
}
