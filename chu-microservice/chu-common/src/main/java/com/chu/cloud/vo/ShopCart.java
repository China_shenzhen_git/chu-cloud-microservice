package com.chu.cloud.vo;

import lombok.Data;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @Description:
 * @author: TianShu.CHU
 * @CreateDate: 2018-05-07 22:48
 * @Version: 1.0
 */
@Data
public class ShopCart extends ShopInfo{

    /**用户可用平台或店铺优惠券**/
    private List<MemberCouponVo> memberCoupons;

    List<CartItem> cartItems;

    /**key:店铺Id,value:店铺下购物车项的商品总价**/
    private Map<Integer,BigDecimal> shopCartPrice = new HashMap<>();


    public void computePrice(){
        if (!CollectionUtils.isEmpty(cartItems)){
            List<CartItem> effectiveList = cartItems.stream().filter(item -> Objects.equals(item.getEffective(), true))
                    .collect(Collectors.toList());
            BigDecimal itemPrice = new BigDecimal(0);
            if (!CollectionUtils.isEmpty(effectiveList)){
                for (CartItem cartItem : effectiveList) {
                    BigDecimal price = cartItem.getProductPrice().multiply(new BigDecimal(cartItem.getQuantity()));
                    itemPrice  = itemPrice.add(price);
                }
            }
            shopCartPrice.put(this.getId(),itemPrice);
        }
    }

}
