package com.chu.cloud.enums;

public enum CouponEnum {

    //领取
    GET,
    //已使用
    USED,
    //过期
    EXPIRED

    ;
}
