package com.chu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class ChuEurekaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChuEurekaApplication.class, args);
	}
}
