#admin
微服务监控管理界面

# hystrix-dashboard
使用turbine组件聚合服务监控中心,一个界面监控同时监控多个服务,并
接入统一配置中心来动态设定需要监控的微服务
curl -X POST http://localhost:8888/bus/refresh

#zipkin
分布式链路跟踪,服务器图形化界面搭建参考官网https://zipkin.io/pages/quickstart.html
采用docker的方式部署:
docker run -d -p 9411:9411 openzipkin/zipkin

