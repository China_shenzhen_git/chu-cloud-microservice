package com.chu.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChuElasticsearchApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChuElasticsearchApplication.class, args);
	}
}
