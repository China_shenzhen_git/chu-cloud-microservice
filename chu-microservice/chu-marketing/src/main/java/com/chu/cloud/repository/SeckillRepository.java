package com.chu.cloud.repository;

import com.chu.cloud.entity.Seckill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/5/24 11:30
 * @Description:
 */
public interface SeckillRepository extends JpaRepository<Seckill, Integer> {


    Seckill findByActIdAndProductId(Integer actId, Integer productId);

    @Modifying
    @Query("update Seckill set seckillInventory = seckillInventory - ?3 where actId = ?1 and productId = ?2 and seckillInventory - ?3 >=0")
    int reduceInventory(Integer actId, Integer productId, Integer quantity);
}
