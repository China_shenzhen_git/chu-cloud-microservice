package com.chu.cloud.entity;

import com.chu.cloud.base.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@ApiModel(value = "discount_coupon")
@Data
public class DiscountCoupon extends AbstractEntity implements Serializable {


    private static final long serialVersionUID = 4624889365056684950L;


    @ApiModelProperty(value = "店铺ID（0 默认平台）", required = true)
    @NotNull
    @Column(name = "shop_id", length = 20)
    private Integer shopId;


    @ApiModelProperty(value = "优惠劵名称")
    @Column(name = "discount_name", length = 100)
    private String discountName;

    @ApiModelProperty(value = "张数数量")
    @Column(name = "number", length = 10)
    private Integer number;


    @ApiModelProperty(value = "条件（满多少）")
    @Column(name = "condition_money", length = 10)
    private BigDecimal conditionMoney;


    @ApiModelProperty(value = "优惠（减多少）")
    @Column(name = "discount_money", length = 10)
    private BigDecimal discountMoney;


    @ApiModelProperty(value = "领取开始时间")
    @Column(name = "start_time", length = 10)
    private Date startTime;


    @ApiModelProperty(value = "领取结束时间")
    @Column(name = "end_time", length = 10)
    private Date endTime;


    @ApiModelProperty(value = "有效期 开始时间")
    @Column(name = "effect_start_time")
    private Date effectStartTime;


    @ApiModelProperty(value = "有效期 结束时间")
    @Column(name = "effect_end_time")
    private Date effectEndTime;


    @ApiModelProperty(value = " 每人限领")
    @Column(name = "limit_receive", length = 11)
    private Integer limitReceive;


    @ApiModelProperty(value = "状态（1 未开始，2 发行中，3 过期）", required = true)
    @Column(name = "status", length = 10)
    private Integer status;


    @ApiModelProperty(value = "版本", required = true)
    @Column(name = "version", length = 10)
    @Version
    private Integer version;


}