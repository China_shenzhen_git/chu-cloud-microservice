package com.chu.cloud.entity;

import com.chu.cloud.base.AbstractEntity;
import com.chu.cloud.enums.ActivityType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "activity")
@ApiModel(value = "activity")
@Data
public class Activity extends AbstractEntity implements Serializable {
    private static final long serialVersionUID = 7557303580617543976L;



    @ApiModelProperty(value = "活动类型", required = true)
    @Column(name = "act_type")
    @Enumerated(EnumType.STRING)
    @NotEmpty(message = "活动类型不能为空")
    private ActivityType actType;


    /**
     * 店铺SPU报名数
     */
    @ApiModelProperty(value = "店铺SPU报名数")
    @Column(name = "sign_num", length = 10)
    private Integer signNum;

    /**
     * 最大参加活动库存
     */
    @ApiModelProperty(value = "最大参加活动库存")
    @Column(name = "sku_max_num", length = 5)
    private Integer skuMaxNum;


    /**
     * 报名开始时间
     */
    @ApiModelProperty(value = "报名开始时间", required = true)
    @Column(name = "sign_start_time", length = 19)
    private Date signStartTime;

    /**
     * 报名结束时间
     */
    @ApiModelProperty(value = "报名结束时间", required = true)
    @Column(name = "sign_end_time", length = 19)
    private Date signEndTime;

    /**
     * 活动开始时间
     */
    @ApiModelProperty(value = "活动开始时间", required = true)
    @Column(name = "start_time", length = 19)
    private Date startTime;

    /**
     * 活动结束时间
     */
    @ApiModelProperty(value = "活动结束时间", required = true)
    @Column(name = "end_time", length = 19)
    private Date endTime;


    /**
     * 活动状态 0:为关闭 1:(默认)为开启
     */
    @ApiModelProperty(value = "活动状态 0:为关闭 1:(默认)为开启", required = true)
    @Column(name = "act_state", length = 3)
    private Integer actState;


}