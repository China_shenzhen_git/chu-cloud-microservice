package com.chu.cloud.dto;


import lombok.Data;

import java.io.Serializable;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/5/30 14:45
 * @Description:
 */
@Data
public class ActivityTagDto implements Serializable{

    private Integer productId;

    private String tag;

    private int expireTime;
}
