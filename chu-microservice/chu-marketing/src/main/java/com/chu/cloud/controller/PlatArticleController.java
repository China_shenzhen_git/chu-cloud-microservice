package com.chu.cloud.controller;

import com.chu.cloud.entity.Article;
import com.chu.cloud.repository.mongo.ArticleRepository;
import com.chu.cloud.service.impl.PlatArticleService;
import com.chu.cloud.tag.TagTools;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/4/13 10:09
 * @Description: 文章控制层
 */
@RestController
@RequestMapping("/v1/article")
@Api(value = "文章相关api")
public class PlatArticleController {

    @Autowired
    private PlatArticleService articleService;

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private TagTools tagTools;

    @PostMapping("/pub")
    public ResponseEntity<Article> pub(@RequestBody Article article) {

        return articleService.pubArticle(article);

    }

    @GetMapping("/{id}")
    @ApiOperation(value = "获取一篇文章",httpMethod = "GET")
    public Map<String,Object> findById(@PathVariable String id){

        Article article = articleRepository.findOne(id);
//        Collection<TagBean> tagBeans = tagTools.generateTags(article.getTitle(), article.getArticle());
        Map<String,Object> resultMap = new HashMap<>();
//        resultMap.put("tags",tagBeans);
        resultMap.put("article",article);

        return resultMap;

    }

}
