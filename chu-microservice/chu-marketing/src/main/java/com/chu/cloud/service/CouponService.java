package com.chu.cloud.service;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/5/3 17:03
 * @Description:
 */
public interface CouponService {

    /**
     * 清除过了领取结束时间的店铺优惠券
     */
    void cleanExpiredCoupon();

    /**
     * 用户领取优惠券
     * @param userId
     * @param couponId
     * @return
     */
    Integer userGetCoupon(Integer userId, Integer couponId);

    /**
     *  优惠券数量 -1
     * @param couponId
     */
    void reduce(Integer couponId);
}
