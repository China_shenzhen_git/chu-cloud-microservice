package com.chu.cloud.repository;

import com.chu.cloud.entity.Activity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/4/20 11:05
 * @Description:
 */
@Repository
public interface ActivityRepository extends JpaRepository<Activity,Integer>{
}
