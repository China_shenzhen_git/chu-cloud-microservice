package com.chu.cloud.repository.mongo;

import com.chu.cloud.entity.Article;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;


/**
 * @author: Tianshu.CHU
 * @Date: 2018/4/13 11:02
 * @Description:
 */
public interface ArticleRepository extends MongoRepository<Article,String>{


}
