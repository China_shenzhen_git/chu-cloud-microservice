package com.chu.cloud.consumer;

import com.chu.cloud.service.CouponService;
import com.chu.cloud.service.impl.CouponServiceImpl;
import com.chu.cloud.stream.channel.CouponNumReduceProcessor;
import com.chu.cloud.stream.consumer.MessageConsumer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;

/**
 * @Description: 用户领取完卡卷, 数量-1
 * @author: TianShu.CHU
 * @CreateDate: 2018-05-04 0:16
 * @Version: 1.0
 */
@Slf4j
@Component
public class CouponReduceMessageConsumer implements MessageConsumer<Integer> {

    @Autowired
    private CouponService couponService;

    @StreamListener(CouponNumReduceProcessor.INPUT)
    @Override
    public void recevieMessage(Integer payLoad) {
        log.info("营销微服务接收到卡卷id:{}消息", payLoad);
        try {
            couponService.reduce(payLoad);

        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }
}
