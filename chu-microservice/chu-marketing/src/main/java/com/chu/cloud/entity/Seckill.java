package com.chu.cloud.entity;

import com.chu.cloud.base.AbstractEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/5/24 10:00
 * @Description:
 */
@Data
@Entity
@Table(indexes = {
        @Index(name = "actId_productId_unique",columnList = "act_id,product_id",unique = true)
})
public class Seckill extends AbstractEntity implements Serializable{

    @Column(name = "act_id",nullable = false)
    private Integer actId;

    @Column(name = "product_id",nullable = false)
    private Integer productId;

    private Integer shopId;

    private BigDecimal price;

    //活动库存
    private Integer seckillInventory;

    //会员限购数
    private Integer skuLimitBuy;



}
