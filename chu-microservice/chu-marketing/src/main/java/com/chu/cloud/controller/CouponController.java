package com.chu.cloud.controller;

import com.chu.cloud.service.CouponService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/5/3 20:27
 * @Description:
 */
@RestController
@RequestMapping("/v1/coupons")
public class CouponController {

    @Autowired
    private CouponService couponService;

    @ApiOperation(value = "用户领取优惠券")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId",value = "用户Id"),
            @ApiImplicitParam(name = "couponId",value = "优惠券id")
    })
    @GetMapping("/get/{userId}/{couponId}")
    public Integer userGetCoupon(@PathVariable Integer userId,@PathVariable Integer couponId){
        Integer result = couponService.userGetCoupon(userId,couponId);
        return result;
    }
}
