package com.chu.cloud.controller;

import com.chu.cloud.entity.Activity;
import com.chu.cloud.entity.DiscountCoupon;
import com.chu.cloud.entity.Seckill;
import com.chu.cloud.service.SeckillService;
import com.chu.cloud.service.impl.PlatActivityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/4/17 17:39
 * @Description: 活动相关
 */
@RestController
@RequestMapping("v1/activity")
@Api(value = "活动相关API")
public class ActivityController {

    @Autowired
    private PlatActivityService activityService;

    @Autowired
    private SeckillService seckillService;


    @PostMapping("/")
    @ApiOperation(value = "发布一条营销活动")
    public Boolean add(@RequestBody @Valid Activity activity){
        return activityService.add(activity);
    }
    @PostMapping("/coupon")
    @ApiOperation(value = "发布优惠券")
    public Integer save(@RequestBody DiscountCoupon coupon){
        return activityService.save(coupon);
    }

    @ApiOperation(value = "发布秒杀活动")
    @PostMapping("/seckill/create")
    public ResponseEntity<Integer> add(@RequestBody Seckill seckill){
        return ResponseEntity.ok(seckillService.save(seckill));
    }

    @ApiOperation(value = "减少秒杀活动库存",hidden = true)
    @PostMapping("/reduce/inventory/{actId}/{productId}/{quantity}")
    public Boolean reduceInventory(@PathVariable Integer actId,@PathVariable Integer productId,@PathVariable Integer quantity){
        return seckillService.reduceInventory(actId,productId,quantity);
    }

}
