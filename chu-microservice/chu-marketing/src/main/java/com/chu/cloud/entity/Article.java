package com.chu.cloud.entity;

import com.chu.cloud.base.BaseEntity;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/4/13 10:10
 * @Description: 文章类
 */
@Document
@Data
public class Article extends BaseEntity<String> {

    private String title;

    private String article;

//    @CreatedBy
    private Integer author;

    private String images;

    /**是否置顶,0否,1置顶**/
    private Integer isTop;

    /**是否发布,0否,1置顶**/
    private Integer isPub;

    /**商品链接json**/
    private String product;





}
