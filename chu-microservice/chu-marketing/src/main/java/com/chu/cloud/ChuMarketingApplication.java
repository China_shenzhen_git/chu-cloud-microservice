package com.chu.cloud;

import com.chu.cloud.stream.channel.CouponNumReduceProcessor;
import com.chu.cloud.stream.channel.OrderPaySuccessProcessor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

@SpringCloudApplication
@EnableBinding({CouponNumReduceProcessor.class, OrderPaySuccessProcessor.class})
@EnableScheduling
@EnableFeignClients
@EnableJpaAuditing
@EnableHystrix
public class ChuMarketingApplication {

    @LoadBalanced
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    public static void main(String[] args) {
        SpringApplication.run(ChuMarketingApplication.class, args);
    }
}
