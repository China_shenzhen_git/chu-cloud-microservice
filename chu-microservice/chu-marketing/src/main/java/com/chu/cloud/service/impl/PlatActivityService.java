package com.chu.cloud.service.impl;

import com.chu.cloud.entity.Activity;
import com.chu.cloud.entity.DiscountCoupon;
import com.chu.cloud.repository.ActivityRepository;
import com.chu.cloud.repository.DiscountCouponRepository;
import com.chu.cloud.util.BeanUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/4/20 11:03
 * @Description:
 */
@Service
@Slf4j
public class PlatActivityService {


    @Autowired
    private ActivityRepository activityRepository;


    @Autowired
    private DiscountCouponRepository couponRepository;

    public Boolean add(Activity activity) {
        activityRepository.save(activity);
        return true;
    }


    public Integer save(DiscountCoupon coupon) {
        if (coupon.getId() != null){
            DiscountCoupon one = couponRepository.findOne(coupon.getId());
            BeanUtil.copyProperties(one,coupon,true);
            DiscountCoupon save = couponRepository.save(one);
            return save.getId();
        }
        DiscountCoupon discountCoupon = couponRepository.save(coupon);
        return discountCoupon.getId();
    }
}
