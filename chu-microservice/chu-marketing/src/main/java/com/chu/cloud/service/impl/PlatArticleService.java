package com.chu.cloud.service.impl;

import com.chu.cloud.entity.Article;
import com.chu.cloud.repository.mongo.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/4/13 11:00
 * @Description:
 */
@Service
public class PlatArticleService {

    @Autowired
    private ArticleRepository articleRepository;

    public ResponseEntity<Article> pubArticle(Article article) {
        Article save = articleRepository.save(article);
        return ResponseEntity.ok(save);

    }


}
