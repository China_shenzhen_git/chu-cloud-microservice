package com.chu.cloud.service;

import com.chu.cloud.entity.Seckill;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/5/24 10:33
 * @Description:
 */
public interface SeckillService {

    /**
     * 店铺报名平台秒杀活动,actId限制了库存数和用户限购数,
     * act_type是活动标签,店铺新参与一个活动,商品对应一个活动标签,
     * 随着活动结束,标签要移除
     * @param seckill
     * @return
     */
    Integer save(Seckill seckill);

    /**
     *  减库存
     * @param productId
     * @param quantity
     * @param actId
     */
    Boolean reduceInventory(Integer actId,Integer productId, Integer quantity);
}
