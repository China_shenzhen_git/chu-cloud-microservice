package com.chu.cloud.service;

import com.chu.cloud.ChuMarketingApplicationTests;
import com.chu.cloud.entity.Activity;
import com.chu.cloud.repository.DiscountCouponRepository;
import com.chu.cloud.service.impl.PlatActivityService;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class ActivityServiceTest extends ChuMarketingApplicationTests {

    @Autowired
    private PlatActivityService activityService;

    @Autowired
    private DiscountCouponRepository couponRepository;

    @Test
    public void add() throws Exception {
        Activity activity = new Activity();
        Boolean flag = activityService.add(activity);
        Assert.assertEquals(flag, false);

    }

    @Test
    public void update() throws Exception {
        int i = couponRepository.updateCoupon(83);
        System.out.println(i);
    }
}