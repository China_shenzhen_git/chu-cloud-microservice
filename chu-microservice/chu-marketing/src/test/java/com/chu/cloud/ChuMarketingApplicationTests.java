package com.chu.cloud;

import com.chu.cloud.dto.MemberCouponDto;
import com.chu.cloud.enums.RedisKeyPrefix;
import com.chu.cloud.rabbit.RabbitConstants;
import com.chu.cloud.rabbit.direct.RabbitDirectSender;
import com.chu.cloud.util.JsonUtils;
import com.chu.cloud.util.RedisUtil;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class ChuMarketingApplicationTests {

	@Autowired
	private RabbitDirectSender directSender;


}
