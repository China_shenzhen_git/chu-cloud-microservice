package com.chu.zuul.test;

import com.chu.cloud.enums.RedisKeyPrefix;
import com.chu.cloud.util.RedisUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/9/13 14:42
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class ChuZuulTest {


    @Test
    public void test(){
        String key = "13786282120";
        Integer integer = RedisUtil.get(RedisKeyPrefix.SMS_HOUR_REQUEST_TIMES, key, Integer.class);
        System.out.println(integer);
    }
}
