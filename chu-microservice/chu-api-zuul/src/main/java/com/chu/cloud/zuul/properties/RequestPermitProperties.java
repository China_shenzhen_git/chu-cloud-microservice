package com.chu.cloud.zuul.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/6/21 17:03
 * @Description:
 */
@ConfigurationProperties(value = "request")
@Data
@Configuration
public class RequestPermitProperties {
    /** 用户黑名单**/
    private List<String> blackList;

    /** 认证白名单**/
    private List<String> authApi;

}
