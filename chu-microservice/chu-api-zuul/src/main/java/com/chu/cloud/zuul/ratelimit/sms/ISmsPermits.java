package com.chu.cloud.zuul.ratelimit.sms;

/**
 *  仅供学习
 *  1条/分钟，5条/小时 ，累计10条/天
 * @author: Tianshu.CHU
 * @Date: 2018/9/11 10:21
 * @Description: 基础的短信限流许可接口
 */
public interface ISmsPermits {

    int HOUR_TIMES = 5;
    int DAY_TIMES = 10;

    /**
     *  isPermit方法返回false时的说明
     * @return
     */
    default String message(){ return "触发流控";}

    /**
     * 核心方法,是否许可
     *
     * @param mobile : 手机号
     * @return true : 允许请求
     *         false: 禁止
     */
    boolean isPermit(String mobile);
}
