package com.chu.cloud.zuul.ratelimit.sms;

import com.chu.cloud.enums.RedisKeyPrefix;
import com.chu.cloud.util.RedisUtil;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;

/**
 *  分钟级流控
 * @author: Tianshu.CHU
 * @Date: 2018/9/11 10:48
 */
@Slf4j
public class MinutesLimitPermits extends SmsPermits{

    @Override
    public String message() {
        return "触发分级流控";
    }

    @Override
    public int permitOrder() {
        return -1;
    }

    @Override
    public boolean isPermit(String mobile) {
        log.info("---->{}进入分钟级短信流控",mobile);
        Boolean isLock = RedisUtil.get(RedisKeyPrefix.SMS_MINUTE_LIMIT_LOCK, mobile, Boolean.class);
        if (Objects.equals(isLock,true)){
            return false;
        }
        RedisUtil.set(RedisKeyPrefix.SMS_MINUTE_LIMIT_LOCK, mobile,true,60);
        return true;
    }
}
