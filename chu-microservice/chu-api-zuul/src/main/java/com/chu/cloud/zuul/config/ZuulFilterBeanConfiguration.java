package com.chu.cloud.zuul.config;

import com.chu.cloud.zuul.filter.error.ZuulExceptionFilter;
import com.chu.cloud.zuul.filter.pre.AccessPermitFilter;
import com.chu.cloud.zuul.filter.pre.SmsCodeRateLimitFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *  过滤器自动装配类
 * @author: Tianshu.CHU
 * @Date: 2018/9/13 10:49
 */
@Configuration
public class ZuulFilterBeanConfiguration {

    @Bean
    public ZuulExceptionFilter exceptionFilter(){
        return new ZuulExceptionFilter();
    }

    @Bean
    public AccessPermitFilter accessPermitFilter(){
        return new AccessPermitFilter();
    }

    @Bean
    public SmsCodeRateLimitFilter smsCodeRateLimitFilter(){
        return new SmsCodeRateLimitFilter();
    }

}
