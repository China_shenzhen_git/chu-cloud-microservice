package com.chu.cloud.zuul.ratelimit.sms;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Collections;

import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

/**
 * ISmsPermits抽象类,提供排序方法
 *
 * @author: Tianshu.CHU
 * @Date: 2018/9/11 10:27
 */
public abstract class SmsPermits implements ISmsPermits, Comparable<SmsPermits> {

    /**
     * 为短信许可器定义许可执行顺序,值越低,执行度越高
     * @return
     */
    public abstract int permitOrder();


    @Override
    public int compareTo(SmsPermits smsPermits) {
        return Integer.compare(this.permitOrder(),smsPermits.permitOrder());
    }

    public static class TestUnit {
        @Mock
        private SmsPermits f1;
        @Mock
        private SmsPermits f2;

        @Before
        public void before() {
            MockitoAnnotations.initMocks(this);
        }

        @Test
        public void testSort() {

            when(f1.permitOrder()).thenReturn(1);
            when(f2.permitOrder()).thenReturn(10);
            when(f1.compareTo(any(SmsPermits.class))).thenCallRealMethod();
            when(f2.compareTo(any(SmsPermits.class))).thenCallRealMethod();

            ArrayList<SmsPermits> list = new ArrayList<SmsPermits>();
            list.add(f2);
            list.add(f1);

            Collections.sort(list);
            System.out.println(list);
            assertSame(f1, list.get(0));
        }
    }
}
