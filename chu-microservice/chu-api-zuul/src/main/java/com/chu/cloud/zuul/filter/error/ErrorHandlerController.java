package com.chu.cloud.zuul.filter.error;

import com.chu.cloud.response.ResultVo;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/9/13 11:02
 */
@RestController
public class ErrorHandlerController implements ErrorController {


    @Override
    public String getErrorPath() {
        return "/error";
    }

    @RequestMapping("/error")
    public ResultVo error(HttpServletRequest request) {
        Throwable throwable = RequestContext.getCurrentContext().getThrowable();
        Throwable cause = throwable.getCause();
        if (cause instanceof ZuulException) {
            ZuulException exception = (ZuulException) cause;
            return ResultVo.getError(exception.nStatusCode, exception.errorCause);
        }
        return ResultVo.getError(HttpStatus.BAD_GATEWAY.value(), "网关出错啦");

    }
}
