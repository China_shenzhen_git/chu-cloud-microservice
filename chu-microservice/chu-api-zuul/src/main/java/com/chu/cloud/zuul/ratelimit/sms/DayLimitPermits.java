package com.chu.cloud.zuul.ratelimit.sms;

import com.chu.cloud.enums.RedisKeyPrefix;
import com.chu.cloud.util.RedisUtil;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;

/**
 * 天级短信流控
 *
 * @author: Tianshu.CHU
 * @Date: 2018/9/11 10:55
 */
@Slf4j
public class DayLimitPermits extends SmsPermits {

    @Override
    public String message() {
        return "触发天级流控";
    }

    @Override
    public int permitOrder() {

        return 1;
    }

    @Override
    public boolean isPermit(String mobile) {
        log.info("--->{}进入天级短信流控", mobile);
        Integer times = RedisUtil.get(RedisKeyPrefix.SMS_DAY_REQUEST_TIMES, mobile, Integer.class);
        times = Objects.isNull(times) ? 0 : times;
        if (times > DAY_TIMES){
            return false;
        }
        String key = RedisKeyPrefix.SMS_DAY_REQUEST_TIMES.getPrefix() + mobile;
        RedisUtil.incr(key,1,60 * 60 * 24);
        return true;
    }
}
