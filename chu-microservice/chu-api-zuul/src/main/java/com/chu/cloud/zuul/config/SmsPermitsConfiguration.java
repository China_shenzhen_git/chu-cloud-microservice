package com.chu.cloud.zuul.config;

import com.chu.cloud.zuul.ratelimit.sms.DayLimitPermits;
import com.chu.cloud.zuul.ratelimit.sms.HourLimitPermits;
import com.chu.cloud.zuul.ratelimit.sms.MinutesLimitPermits;
import com.chu.cloud.zuul.ratelimit.sms.SmsPermits;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *  自动装配类
 * @author: Tianshu.CHU
 * @Date: 2018/9/11 11:15
 */
@Configuration
public class SmsPermitsConfiguration {

    @Bean
    public SmsPermits minutesPermits(){
        return new MinutesLimitPermits();
    }

    @Bean
    public SmsPermits hourPermits(){
        return new HourLimitPermits();
    }

    @Bean
    public SmsPermits dayPermits(){
        return new DayLimitPermits();
    }
}
