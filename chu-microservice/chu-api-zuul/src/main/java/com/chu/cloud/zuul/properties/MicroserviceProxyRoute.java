package com.chu.cloud.zuul.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/6/15 10:19
 * @Description: 微服务代理请求
 */
@ConfigurationProperties(prefix = "microservice.route")
@Data
@Component
public class MicroserviceProxyRoute {

    private String openService;

}
