package com.chu.cloud.zuul.config;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;

import java.util.ArrayList;
import java.util.List;

@Component
@Primary
public class DocumentationConfig implements SwaggerResourcesProvider {
    @Override
    public List<SwaggerResource> get() {
        List resources = new ArrayList<>();
        resources.add(swaggerResource("订单系统", "/chu-order/v2/api-docs", "1.0"));
        resources.add(swaggerResource("营销系统", "/chu-marketing/v2/api-docs", "1.0"));
        resources.add(swaggerResource("商品系统", "/chu-product/v2/api-docs", "1.0"));
        resources.add(swaggerResource("用户系统", "/chu-user/v2/api-docs", "1.0"));
        resources.add(swaggerResource("聚合系统", "/api/v2/api-docs", "1.0"));
        return resources;
    }

    private SwaggerResource swaggerResource(String name, String location, String version) {
        SwaggerResource swaggerResource = new SwaggerResource();
        swaggerResource.setName(name);
        swaggerResource.setLocation(location);
        swaggerResource.setSwaggerVersion(version);
        return swaggerResource;
    }
}