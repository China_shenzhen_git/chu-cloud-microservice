package com.chu.cloud.zuul.filter.error;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import lombok.extern.slf4j.Slf4j;

/**
 * @author: Tianshu.CHU
 * @Date: 2018/9/13 10:09
 */
@Slf4j
public class ZuulExceptionFilter extends ZuulFilter {
    @Override
    public String filterType() {
        return "error";
    }

    @Override
    public int filterOrder() {
        return 100;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        Throwable throwable = RequestContext.getCurrentContext().getThrowable();
        log.error("进入系统异常过滤器:", throwable.getCause().getMessage());
        return null;
    }
}
