package com.chu.cloud.zuul.ratelimit;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


/**
 * @author: Tianshu.CHU
 * @Date: 2018/7/19 11:18
 * @Description:
 */
@ConfigurationProperties(prefix = SmsRateLimitProperties.PREFIX)
@Component
@Data
public class SmsRateLimitProperties {
    public static final String PREFIX = "zuul.ratelimite";

    private boolean enable;

    private String limitKey;

    private Integer limitMax;


}
