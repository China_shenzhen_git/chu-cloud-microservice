# chu-cloud-microservice

## 项目介绍

基于Spring Cloud Stream消息驱动型高并发架构微服务,集成docker容器化部署,熔断,接口请求隔离操作,服务降级,rabbitmq,mongodb,redis等


## 软件架构

#### 1.chu-api-zuul 集成网关,请求转发,swagger集成,统一认证,黑名单过滤,限流

#### 2.chu-open-service BFF层,处理高并发请求,聚合API

#### 3.chu-eureka-ha 高可用注册中心,提供docker-compose集群部署

#### 4.chu-config 配置服务中心,动态拉取,环境切换等

#### 5.config-repo 配置文件仓储

#### 6.chu-monitor 监控服务,提供hystrix熔断监控,zipkin分布式链路跟踪,admin UI监控界面化

#### 7.chu-common 公共包,集成feign,rabbitmq,redis,mongodb等

#### 8.chu-user 用户微服务

#### 9.chu-product 商品店铺微服务

#### 10.chu-order 订单微服务

#### 11.chu-marketing 营销微服务

#### 12.chu-elasticsearch 搜索引擎待集成

## 安装教程

1. 阅读每个项目含有的readme.md文件

2. 将config-repo项目发布到你的码云,在chu-config配置文件中指定该远程url

3. 修改config-repo文件库里的数据库连接、redis连接、mongodb连接、rabbitmq连接信息换成自己的

4. 配置eureka注册中心地址

4. 新建chu_marketing_db,chu_order_db,chu_product_db,chu_user_db数据库

5. 先启动eureka,再启动chu-config服务,再启动基础微服务(拥有自动建表功能)


## 后期准备

1.版本迁移至springboot2.0和spring cloud Finchley 版本

2.集成搜索引擎elasticsearch

3.集成TCC分布式事务案例

